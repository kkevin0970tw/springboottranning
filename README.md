### 本專案使用框架版本 : Spring Boot 3.2.4
#### JAVA環境 : JDK 21
#### 套件管理 : Maven
#### 資料庫: MySQL
#### 伺服器: Tomcat

### 練習目標技術 :
#### Spring AOP : 切面應用
#### Spring Data JPA : CRUD資料
#### Spring Security : 密碼加密、登入帳號權限、頁面訪問權限
#### Quartz  : 定時任務
#### Spring Cache (Caffeine) / Redis Cache : 搭配做二級緩存
#### RabbitMQ : 發布訂閱效果 (本專案角色為:生產者)
#### Swagger 3 : API文件、測試API UI介面
#### Junit5 : 測試程式
#### Thymeleaf : 前端模板

## 專案規劃(以能正常使用技術為目標，故使用上不一定符合真實情境)
- AOP 切面訊息
  - `@Before` & `@AfterReturning` 紀錄切點 方法執行前 與 方法回傳後兩時間點的內容
- Exception 處理
  - `@ControllerAdvice` 與 `@ExceptionHandler` 搭配 達到統一管理
  - 以下三個java檔達到 Builder模式 
    - [ExceptionsInterface.java](src%2Fmain%2Fjava%2Fcom%2Fhengwei%2Fside2%2Fexception%2FExceptionsInterface.java)
    - [ExceptionsBuilder.java](src%2Fmain%2Fjava%2Fcom%2Fhengwei%2Fside2%2Fexception%2FExceptionsBuilder.java)
    - [Exceptions.java](src%2Fmain%2Fjava%2Fcom%2Fhengwei%2Fside2%2Fexception%2FExceptions.java)
- JPA 應用
  - `@EmbeddedId` 綁定雙主鍵
  -  透過 `xxxEntity implements Persistable<Integer>` 達到標記該entity是否為新紀錄，影響 `save()`是否 `select` 後才`update`
  - `@Transactional` 回滾事件
- Spring Security 應用  
  - 帳號權重分為 Root > Admin > User5 ~ User1   (大>小)
  - ` public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) ` 決定整體配置
    - 訪問頁面限制(權重高可訪問權重低的頁面) ex: `user3 可訪問 user1 ，反之 user1 不可訪問 user3`
    - PasswordEncoder 加密方式選擇
    - [UserDetailServiceImpl.java](src%2Fmain%2Fjava%2Fcom%2Fhengwei%2Fside2%2Fsecurity%2Fservice%2FUserDetailServiceImpl.java) 改寫 loadUserByUsername() 取得登入者資訊 及 決定權限
    - [AuthenticationSuccessHandler.java](src%2Fmain%2Fjava%2Fcom%2Fhengwei%2Fside2%2Fsecurity%2Fhandler%2FAuthenticationSuccessHandler.java) 決定成功登入後 對應權重帳號 自動前往對應頁面
- Quartz 應用
  - 配置儲存方式為 : JDBC
  - 需先於 MySQL 建立好 Table (官方lib有提供各種資料庫的語法) 需載入pom檔 存於 :`org.quartz.impl.jdbcjobstore`
  - 應用上以 [固定頻率產生交易](src%2Fmain%2Fjava%2Fcom%2Fhengwei%2Fside2%2Fjob%2FAutoBuyJob.java) 及 [固定時間產生每日報表](src%2Fmain%2Fjava%2Fcom%2Fhengwei%2Fside2%2Fjob%2FDailyReportBuildJob.java) 為實作
  - 兩種頻率配置方式 :
    - `SimpleScheduleBuilder.repeatSecondlyForever(10)`
    - `CronScheduleBuilder.cronSchedule("0 30 1 * * ?")`
- Spring Cache (Caffeine) / Redis Cache
  - 二級緩存 : 取資料順序  Caffeine Cache → Redis Cache → MySQL
- RabbitMQ 應用
  - [RabbitMqEnum.java](src%2Fmain%2Fjava%2Fcom%2Fhengwei%2Fside2%2Fenumeration%2FRabbitMqEnum.java) 搭配   [RabbitMqConfig.java](src%2Fmain%2Fjava%2Fcom%2Fhengwei%2Fside2%2Fconfig%2FRabbitMqConfig.java) 做動態配置
  - 本專案透過 新增產品/更新產品 作為觸發 發布訊息
  - 有不同 Exchange 及 Queue ，需建立額外 消費者端(多個/一個) 
  - 可透過UI介面查看 目前 發布訂閱狀態
- Swagger 3
  - `@Tag` : 定義Controller 名稱/敘述 
  - `@Operation` : 定義 Controller Method 概述
  - `@Schema` : 定義 vo 名稱 及 屬性的資訊
  - UI介面 http://127.0.0.1:8080/api/swagger  需登入root權限帳號
- Junit5
  - 此專案未於此處著墨，僅使用兩種模式為範本
    - 模式一
      1. 定義通用介面 [TestServiceInterface.java](src%2Ftest%2Fjava%2Fservice%2FTestServiceInterface.java)
      2. 定義通用Mock工具(模擬網路呼叫API) [MockTools.java](src%2Ftest%2Fjava%2FUtils%2FMockTools.java)
      3. 透過實作 `TestServiceInterface` 調用 `MockTools` 定義的方法 (如 GET 可透過URL傳遞參數 / POST 可自訂Body)
      4. `解析回傳Json`是否如實
    - 模式二
      1. 建立 Input
      2. 呼叫 Service
      3. `驗證回傳結果物件`是否如實
- Thymeleaf
  - 對以下常用語法做應用
    - th:fragment : 段落定義
    - th:replace : 段落取代
    - th:action : form表單 action
    - th:each : 迴圈
    - ${ } : 參數取得
      - ${x}
      - ${param.x}
- 其他
  - 
 
  
# 專案外練習
#### Azure 雲端虛擬主機 : Linux Server(Ubuntu) 
- Redis Server By Docker

#### VMware 本機上的虛擬主機 : Linux Server(Ubuntu)  - Docker-Compose建置
- Redis Server  
- RabbitMQ Server
- FTP Server (vsftpd)

## # 註明 : 因前端不再此專案重點，故些許頁面僅透過 ChatGPT 提供後 修改而成。

