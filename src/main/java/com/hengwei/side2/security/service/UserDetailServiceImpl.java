package com.hengwei.side2.security.service;

import com.hengwei.side2.entity.ManagerEntity;
import com.hengwei.side2.entity.MemberEntity;
import com.hengwei.side2.enumeration.RoleEnum;
import com.hengwei.side2.exception.Exceptions;
import com.hengwei.side2.repository.ManagerRepository;
import com.hengwei.side2.repository.MemberRepository;
import com.hengwei.side2.utils.OptionalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Arrays;
import java.util.List;

public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private ManagerRepository managerRepository;
    @Autowired
    private OptionalUtils<ManagerEntity> managerEntityOptionalUtils;
    @Autowired
    private OptionalUtils<MemberEntity> memberEntityOptionalUtils;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String account;
        String password;
        int roleId;
        if (username.startsWith("manager@")) {
            ManagerEntity managerEntity = getManager(username);
            account = managerEntity.getAccount();
            password = managerEntity.getPassword();
            roleId = managerEntity.getRoleId();
        } else {
            MemberEntity memberEntity = getMember(username);
            account = memberEntity.getAccount();
            password = memberEntity.getPassword();
            roleId = memberEntity.getRoleId();
        }
        List<GrantedAuthority> authorityList = getAppropriatePermission(roleId);
        return new User(account, password, authorityList);
    }

    private List<GrantedAuthority> getAppropriatePermission(int roleId) {
        return AuthorityUtils.createAuthorityList(RoleEnum.getAllRoleByRoleId(roleId));
    }

    private MemberEntity getMember(String username) {
        return memberEntityOptionalUtils.findOneOrThrow(
                username,
                memberRepository.findByAccount(username),
                Exceptions.AuthorizationException.AUTHORIZATION_EXCEPTION
        );
    }

    private ManagerEntity getManager(String username) {
        return managerEntityOptionalUtils.findOneOrThrow(
                username,
                managerRepository.findByAccount(username),
                Exceptions.AuthorizationException.AUTHORIZATION_EXCEPTION);
    }
}
