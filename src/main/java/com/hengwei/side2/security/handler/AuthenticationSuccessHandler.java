package com.hengwei.side2.security.handler;

import com.hengwei.side2.enumeration.RoleEnum;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;

public class AuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        // 取得該
        var roleEnum = getHighestPermission(authentication.getAuthorities());
        setDefaultTargetUrl(
                switchTargetUrl(roleEnum)
                        .concat("?username=").concat(authentication.getName())
                        .concat("&role=".concat(roleEnum.name()))
        );
        super.onAuthenticationSuccess(request, response, authentication);
    }

    private RoleEnum getHighestPermission(Collection<? extends GrantedAuthority> authorities) {
        int roleId = 1;
        for (GrantedAuthority authority : authorities) {
            roleId = Math.max(roleId, RoleEnum.valueOf(String.valueOf(authority)).getRoleId());
        }
        return RoleEnum.getRoleByRoleId(roleId);
    }

    private String switchTargetUrl(RoleEnum role) {
        return switch (role) {
            case USER_1 -> "/security/auto-direct/user1";
            case USER_2 -> "/security/auto-direct/user2";
            case USER_3 -> "/security/auto-direct/user3";
            case USER_4 -> "/security/auto-direct/user4";
            case USER_5 -> "/security/auto-direct/user5";
            case ROOT -> "/security/auto-direct/root";
            case ADMIN -> "/security/auto-direct/admin";
        };
    }
}
