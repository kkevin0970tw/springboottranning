package com.hengwei.side2.bootstrap;

import org.springframework.boot.BootstrapContext;
import org.springframework.boot.BootstrapRegistry;

public class TestSupplier implements BootstrapRegistry.InstanceSupplier<TestLoader> {

    @Override
    public TestLoader get(BootstrapContext context) {
        System.out.println(TestSupplier.class.getSimpleName().concat(" 初始化完成"));
        return new TestLoader();
    }
}
