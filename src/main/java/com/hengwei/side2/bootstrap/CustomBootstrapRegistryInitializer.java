package com.hengwei.side2.bootstrap;

import org.springframework.boot.BootstrapRegistry;
import org.springframework.boot.BootstrapRegistryInitializer;
/*
   此階段為 生命週期 [starting] 前
   先透過 SPI 綁定  (META-INF/spring.factories)
   在SpringApplication.run()執行過程 會new SpringApplication
   其中 會找到此 Initializer 並加入 List<BootstrapRegistryInitializer>當中
   該 List 將會 foreach，並執行每個Initializer 的 initialize()
*/
public class CustomBootstrapRegistryInitializer implements BootstrapRegistryInitializer {
    @Override
    public void initialize(BootstrapRegistry registry) {
        // 將物件註冊到 BootstrapContext 的 map當中
        System.out.println("CustomBootstrapRegistryInitializer 執行");
        registry.register(TestLoader.class, new TestSupplier());

        System.out.println("BootstrapContextCloseListener 註冊");
        registry.addCloseListener(new BootstrapContextCloseListener());
    }
}
