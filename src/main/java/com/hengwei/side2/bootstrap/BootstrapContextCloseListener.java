package com.hengwei.side2.bootstrap;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.BootstrapContextClosedEvent;
import org.springframework.context.ApplicationListener;

public class BootstrapContextCloseListener implements ApplicationListener<BootstrapContextClosedEvent> {
    @Override
    public void onApplicationEvent(BootstrapContextClosedEvent event) {
        System.out.println("BootstrapContextClose Event 發生，代表 IOC容器已準備好");
    }
}
