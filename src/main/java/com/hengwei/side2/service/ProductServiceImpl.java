package com.hengwei.side2.service;

import com.hengwei.side2.entity.*;
import com.hengwei.side2.enumeration.RabbitMqEnum;
import com.hengwei.side2.exception.Exceptions;
import com.hengwei.side2.repository.*;
import com.hengwei.side2.utils.CacheUtils;
import com.hengwei.side2.utils.OptionalUtils;
import com.hengwei.side2.utils.RabbitMqUtils;
import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.*;
import com.hengwei.side2.vo.output.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private OptionalUtils<ProductEntity> optionalUtils;
    @Autowired
    private RabbitMqUtils rabbitMqUtils;
    @Autowired
    private CacheUtils<CategoryEntity> categoryCacheUtils;
    @Autowired
    private CacheUtils<WineSubTypeEntity> wineSubTypeCacheUtils;
    @Autowired
    private CacheUtils<CountryEntity> countryCacheUtils;
    @Autowired
    private CacheUtils<AreaEntity> areaCacheUtils;
    @Autowired
    private CacheUtils<BrandEntity> brandCacheUtils;
    @Autowired
    private CacheUtils<GrapeVarietiesEntity> grapeVarietiesCacheUtils;
    @Autowired
    private CacheUtils<OakEntity> oakCacheUtils;

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private WineSubTypeRepository wineSubTypeRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private AreaRepository areaRepository;
    @Autowired
    private BrandRepository brandRepository;
    @Autowired
    private GrapeVarietiesRepository grapeVarietiesRepository;
    @Autowired
    private OakRepository oakRepository;
    @Autowired
    private ProductRepository productRepository;

    @Override
    public APIResult<GetProductByIdOutput> getProductById(GetProductByIdInput input) {
        ProductEntity productEntity = optionalUtils.findOneOrThrow(
                input,
                productRepository.findById(input.getId()),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );
        GetProductByIdOutput output = new GetProductByIdOutput();
        setOutput(output, productEntity);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetProductsWithPageOutput> getProductsWithPage(GetProductsWithPageInput input) {
        Page<ProductEntity> productEntityPage = optionalUtils.findMoreWithPageOrThrow(
                input,
                productRepository.findAll(PageRequest.of(input.getTargetPage() - 1, input.getPerPageQuantity())),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );
        GetProductsWithPageOutput output = new GetProductsWithPageOutput();
        output.setDataList(getDataList(productEntityPage));
        output.setPageParamOutput(productEntityPage);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetProductsByCategoryWithPageOutput> getProductsByCategoryWithPage(GetProductsByCategoryWithPageInput input) {
        Page<ProductEntity> productEntityPage = optionalUtils.findMoreWithPageOrThrow(
                input,
                productRepository.findAllByCategoryId(input.getCategoryId(), PageRequest.of(input.getTargetPage() - 1, input.getPerPageQuantity())),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );
        GetProductsByCategoryWithPageOutput output = new GetProductsByCategoryWithPageOutput();
        output.setDataList(getDataList(productEntityPage));
        output.setPageParamOutput(productEntityPage);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetProductsByWineSubTypeWithPageOutput> getProductsByWineSubTypeWithPage(GetProductsByWineSubTypeInputWithPage input) {
        Page<ProductEntity> productEntityPage = optionalUtils.findMoreWithPageOrThrow(
                input,
                productRepository.findAllByWineSubTypeId(input.getWineSubTypeId(), PageRequest.of(input.getTargetPage() - 1, input.getPerPageQuantity())),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );
        GetProductsByWineSubTypeWithPageOutput output = new GetProductsByWineSubTypeWithPageOutput();
        output.setDataList(getDataList(productEntityPage));
        output.setPageParamOutput(productEntityPage);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetProductsByCountryWithPageOutput> getProductsByCountryWithPage(GetProductsByCountryWithPageInput input) {
        Page<ProductEntity> productEntityPage = optionalUtils.findMoreWithPageOrThrow(
                input,
                productRepository.findAllByCountryId(input.getCountryId(), PageRequest.of(input.getTargetPage() - 1, input.getPerPageQuantity())),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );
        GetProductsByCountryWithPageOutput output = new GetProductsByCountryWithPageOutput();
        output.setDataList(getDataList(productEntityPage));
        output.setPageParamOutput(productEntityPage);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetProductsByAreaWithPageOutput> getProductsByAreaWithPage(GetProductsByAreaWithPageInput input) {
        Page<ProductEntity> productEntityPage = optionalUtils.findMoreWithPageOrThrow(
                input,
                productRepository.findAllByAreaId(input.getAreaId(), PageRequest.of(input.getTargetPage() - 1, input.getPerPageQuantity())),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );
        GetProductsByAreaWithPageOutput output = new GetProductsByAreaWithPageOutput();
        output.setDataList(getDataList(productEntityPage));
        output.setPageParamOutput(productEntityPage);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetProductsByBrandWithPageOutput> getProductsByBrandWithPage(GetProductsByBrandWithPageInput input) {
        Page<ProductEntity> productEntityPage = optionalUtils.findMoreWithPageOrThrow(
                input,
                productRepository.findAllByBrandId(input.getBrandId(), PageRequest.of(input.getTargetPage() - 1, input.getPerPageQuantity())),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );
        GetProductsByBrandWithPageOutput output = new GetProductsByBrandWithPageOutput();
        output.setDataList(getDataList(productEntityPage));
        output.setPageParamOutput(productEntityPage);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetProductsByGrapeVarietiesWithPageOutput> getProductsByGrapeVarietiesWithPage(GetProductsByGrapeVarietiesWithPageInput input) {
        Page<ProductEntity> productEntityPage = optionalUtils.findMoreWithPageOrThrow(
                input,
                productRepository.findAllByGrapeVarietiesId(input.getGrapeVarietiesId(), PageRequest.of(input.getTargetPage() - 1, input.getPerPageQuantity())),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );
        GetProductsByGrapeVarietiesWithPageOutput output = new GetProductsByGrapeVarietiesWithPageOutput();
        output.setDataList(getDataList(productEntityPage));
        output.setPageParamOutput(productEntityPage);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetProductsByOakWithPageOutput> getProductsByOakWithPage(GetProductsByOakWithPageInput input) {
        Page<ProductEntity> productEntityPage = optionalUtils.findMoreWithPageOrThrow(
                input,
                productRepository.findAllByOakId(input.getOakId(), PageRequest.of(input.getTargetPage() - 1, input.getPerPageQuantity())),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );
        GetProductsByOakWithPageOutput output = new GetProductsByOakWithPageOutput();
        output.setDataList(getDataList(productEntityPage));
        output.setPageParamOutput(productEntityPage);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetProductsByPriceRangeWithPageOutput> getProductsByPriceRangeWithPage(GetProductsByPriceRangeWithPageInput input) {
        Page<ProductEntity> productEntityPage = optionalUtils.findMoreWithPageOrThrow(
                input,
                productRepository.findAllByPriceBetween(
                        input.getPriceFloor(),
                        input.getPriceCap(),
                        PageRequest.of(
                                input.getTargetPage() - 1,
                                input.getPerPageQuantity(),
                                Sort.by(Sort.Direction.valueOf(input.getSort()), "price"))
                ),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );
        GetProductsByPriceRangeWithPageOutput output = new GetProductsByPriceRangeWithPageOutput();
        output.setDataList(getDataList(productEntityPage));
        output.setPageParamOutput(productEntityPage);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<UpdateProductOutput> updateProduct(UpdateProductInput input) {
        ProductEntity productEntity = optionalUtils.findOneOrThrow(
                input,
                productRepository.findById(input.getId()),
                Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
        );
        BeanUtils.copyProperties(input, productEntity);
        ProductEntity updateProductEntity = productRepository.saveAndFlush(productEntity);

        UpdateProductOutput output = new UpdateProductOutput();
        BeanUtils.copyProperties(updateProductEntity, output);
        rabbitMqUtils.sendMessage(RabbitMqEnum.PRODUCT_EVENT, "產品已更新!!".concat(output.toString()));
        return new APIResult<>(output);
    }

    @Override
    public APIResult<DeleteProductOutput> deleteProduct(DeleteProductInput input) {
        ProductEntity productEntity = optionalUtils.findOneOrThrow(
                input,
                productRepository.findById(input.getId()),
                Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
        );
        productRepository.delete(productEntity);
        return new APIResult<>();
    }

    @Override
    public APIResult<AddProductOutput> addProduct(AddProductInput input) {
        Integer nextId = productRepository.getNextIdVal();
        optionalUtils.existThenThrow(
                input,
                productRepository.findById(nextId),
                Exceptions.DataBaseException.DATA_BASE_DATA_ALREADY_EXIST
        );

        ProductEntity productEntity = new ProductEntity();
        BeanUtils.copyProperties(input, productEntity);
        productEntity.markNew();

        ProductEntity newProductEntity = productRepository.saveAndFlush(productEntity);
        AddProductOutput output = new AddProductOutput();
        BeanUtils.copyProperties(newProductEntity, output);

        rabbitMqUtils.sendMessage(RabbitMqEnum.PRODUCT_ADDED, "新產品發布囉!!".concat(output.toString()));
        return new APIResult<>(output);
    }

    private List<BasicProductOutput> getDataList(Page<ProductEntity> productEntityPage) {
        return productEntityPage.
                getContent()
                .stream()
                .map(this::mapToOutput)
                .collect(Collectors.toList());
    }

    private BasicProductOutput mapToOutput(ProductEntity productEntity) {
        BasicProductOutput output = new BasicProductOutput();
        setOutput(output, productEntity);
        return output;
    }

    private void setOutput(BasicProductOutput output, ProductEntity productEntity) {
        output.setId(productEntity.getId());
        output.setName(productEntity.getName());
        output.setCategory(categoryCacheUtils.getEntityFromCache(categoryRepository, productEntity.getCategoryId(), CategoryEntity.class));
        output.setWineSubType(wineSubTypeCacheUtils.getEntityFromCache(wineSubTypeRepository, productEntity.getWineSubTypeId(), WineSubTypeEntity.class));
        output.setCountry(countryCacheUtils.getEntityFromCache(countryRepository, productEntity.getCountryId(), CountryEntity.class));
        output.setArea(areaCacheUtils.getEntityFromCache(areaRepository, productEntity.getAreaId(), AreaEntity.class));
        output.setBrand(brandCacheUtils.getEntityFromCache(brandRepository, productEntity.getBrandId(), BrandEntity.class));
        output.setGrapeVarieties(grapeVarietiesCacheUtils.getEntityFromCache(grapeVarietiesRepository, productEntity.getGrapeVarietiesId(), GrapeVarietiesEntity.class));
        output.setOak(oakCacheUtils.getEntityFromCache(oakRepository, productEntity.getOakId(), OakEntity.class));
        output.setImgPath(productEntity.getImgPath());
        output.setPrice(productEntity.getPrice());
        output.setCapacity(productEntity.getCapacity());
        output.setDescription(productEntity.getDescription());
        output.setShow(productEntity.getShow());
        output.setUpdateDateTime(productEntity.getUpdateDateTime());
        output.setDiscount(productEntity.getDiscount());
    }
}
