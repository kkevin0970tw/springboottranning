package com.hengwei.side2.service;

import com.hengwei.side2.dto.PayTypeCountDTO;
import com.hengwei.side2.dto.ProductSalesDTO;
import com.hengwei.side2.dto.TradeStateDTO;
import com.hengwei.side2.dto.TradeSummaryDTO;
import com.hengwei.side2.entity.*;
import com.hengwei.side2.enumeration.TradeStateEnum;
import com.hengwei.side2.exception.Exceptions;
import com.hengwei.side2.exception.ExceptionsBuilder;
import com.hengwei.side2.repository.*;
import com.hengwei.side2.utils.OptionalUtils;
import com.hengwei.side2.vo.input.PurchaseInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class QuartzJobServiceImpl implements QuartzJobService {

    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private PayTypeRepository payTypeRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private TradeRepository tradeRepository;
    @Autowired
    private TradeDetailRepository tradeDetailRepository;
    @Autowired
    private ReportDailyTradeSummaryRepository reportDailyTradeSummaryRepository;
    @Autowired
    private ReportDailyPayTypeCountRepository reportDailyPayTypeCountRepository;
    @Autowired
    private ReportDailyProductSalesRepository reportDailyProductSalesRepository;
    @Autowired
    private ReportDailyTradeStateRepository reportDailyTradeStateRepository;

    @Autowired
    private OptionalUtils<ProductEntity> productOptionalUtils;

    @Override
    @Transactional
    public void autoBuy() {
        int purchaseItemCount = getRandom(10);
        List<MemberEntity> allMembers = memberRepository.findAll();
        List<PayTypeEntity> allPayType = payTypeRepository.findAll();

        PurchaseInput input = new PurchaseInput();
        input.setMemberId(allMembers.get(getRandom(allMembers.size())).getId());
        input.setPayTypeId(allPayType.get(getRandom(allPayType.size())).getId());
        input.setPurchaseItemList(getPurchaseList(purchaseItemCount));
        input.setTradeDateTime(LocalDateTime.now());

        TradeEntity tradeEntity = saveTrade(input);
        List<TradeDetailEntity> tradeDetailEntityList = saveTradeDetail(tradeEntity, input.getPurchaseItemList());
    }

    private List<PurchaseInput.PurchaseItem> getPurchaseList(int purchaseItemCount) {
        List<PurchaseInput.PurchaseItem> purchaseItemList = new ArrayList<>();
        PurchaseInput.PurchaseItem item;

        List<ProductEntity> allProducts = productRepository.findAll();
        ProductEntity productEntity;
        for (int i = 0; i < purchaseItemCount; i++) {
            item = new PurchaseInput.PurchaseItem();
            productEntity = allProducts.get(getRandom(allProducts.size()));
            item.setProductId(productEntity.getId());
            item.setQuantity(getRandom(50));
            item.setPrice((productEntity.getPrice()));
            item.setPriceWithDiscount((int) (productEntity.getPrice() * productEntity.getDiscount() * 0.01));
            item.setDiscount(productEntity.getDiscount());
            purchaseItemList.add(item);
        }
        return purchaseItemList;
    }

    private TradeEntity saveTrade(PurchaseInput input) {
        TradeEntity tradeEntity = new TradeEntity();
        tradeEntity.setTradeDateTime(LocalDateTime.now());
        tradeEntity.setTradeStateId(TradeStateEnum.values()[getRandom(TradeStateEnum.values().length)].getStateNum());
        tradeEntity.setMemberId(input.getMemberId());
        tradeEntity.setTradeAmount(input.getPurchaseItemList().stream().mapToInt(item -> (int) item.getPrice() * item.getQuantity()).sum());
        tradeEntity.setTradeAmountWithDiscount(input.getPurchaseItemList().stream().mapToInt(item -> (int) (item.getPrice() * item.getQuantity() * item.getDiscount() * 0.01)).sum());
        tradeEntity.setPayTypeId(input.getPayTypeId());
        return tradeRepository.save(tradeEntity);
    }

    private List<TradeDetailEntity> saveTradeDetail(TradeEntity tradeEntity, List<PurchaseInput.PurchaseItem> purchaseItemList) {
        List<TradeDetailEntity> tradeDetailEntityList = new ArrayList<>();
        for (PurchaseInput.PurchaseItem item : purchaseItemList) {
            /*避免時間差 前端使用者尚未更新畫面及售價 導致與資料庫實際售價不同產生資料錯誤*/
            checkProductDataConsistent(item);
            TradeDetailEntity tradeDetailEntity = new TradeDetailEntity();
            tradeDetailEntity.setPk(new TradeDetailPK(tradeEntity.getId(), item.getProductId()));
            tradeDetailEntity.setPrice(item.getPrice());
            tradeDetailEntity.setDiscount(item.getDiscount());
            tradeDetailEntity.setQuantity(item.getQuantity());
            tradeDetailEntityList.add(tradeDetailEntity);
        }
        return tradeDetailRepository.saveAll(tradeDetailEntityList);
    }

    private void checkProductDataConsistent(PurchaseInput.PurchaseItem item) {
        ProductEntity productEntity = productOptionalUtils.findOneOrThrow(
                item,
                productRepository.findById(item.getProductId()),
                Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
        );
        if (!productEntity.getDiscount().equals(item.getDiscount()) || !productEntity.getPrice().equals(item.getPrice())) {
            throw ExceptionsBuilder.builder(Exceptions.DataBaseException.DATA_BASE_CONSISTENT_ERROR)
                    .setErrorLog(item, productRepository)
                    .setCustomMessage("產品資料出現不一致狀態，導致交易失敗，請通知客服")
                    .build();
        }
    }

    private Integer getRandom(int max) {
        int num = ThreadLocalRandom.current().nextInt(max);
        if (num == 0) {
            num += 1;
        }
        return num;
    }

    @Override
    @Transactional
    public void dailyReportBuild() {
        // 每日報表產生
        LocalDateTime now = LocalDateTime.now();
        LocalDate minusOneDay = LocalDate.now().minusDays(1);
        LocalDateTime todayStart = LocalDateTime.now().minusDays(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
        LocalDateTime todayEnd = LocalDateTime.now().minusDays(1).withHour(23).withMinute(59).withSecond(59).withNano(999999999);

        buildDailyTradeSummaryReport(minusOneDay, todayStart, todayEnd);
        buildDailyPayTypeCountReport(minusOneDay, todayStart, todayEnd);
        buildDailyProductSalesReport(minusOneDay, todayStart, todayEnd);
        buildDailyTradeStateReport(minusOneDay, todayStart, todayEnd);
    }

    private void buildDailyTradeSummaryReport(LocalDate minusOneDay, LocalDateTime todayStart, LocalDateTime todayEnd) {
        if (reportDailyTradeSummaryRepository.findById(minusOneDay).isPresent()) {
            return;
        }
        Object[][] queryResultArray = tradeRepository.getTradeSummaryByDateRange(todayStart, todayEnd);
        TradeSummaryDTO tradeSummaryDTO = (TradeSummaryDTO) castNativeSelectResultToDTO(queryResultArray[0], TradeSummaryDTO.class);

        ReportDailyTradeSummaryEntity reportDailyTradeSummaryEntity = new ReportDailyTradeSummaryEntity();
        reportDailyTradeSummaryEntity.setDate(minusOneDay);
        reportDailyTradeSummaryEntity.setTotalAmount(tradeSummaryDTO.getTotalTradeAmount());
        reportDailyTradeSummaryEntity.setTotalCount(tradeSummaryDTO.getTotalCount());
        reportDailyTradeSummaryEntity.setTotalDiscount(tradeSummaryDTO.getTotalTradeAmountWithDiscount());
        reportDailyTradeSummaryRepository.save(reportDailyTradeSummaryEntity);
    }

    private void buildDailyPayTypeCountReport(LocalDate minusOneDay, LocalDateTime todayStart, LocalDateTime todayEnd) {
        if (!reportDailyPayTypeCountRepository.findAllByPkDate(minusOneDay).isEmpty()) {
            return;
        }
        Object[][] queryResultArray = tradeRepository.getPayTypeCountByDateRange(todayStart, todayEnd);
        List<ReportDailyPayTypeCountEntity> reportDailyPayTypeCountEntityList = new ArrayList<>();
        ReportDailyPayTypeCountEntity reportDailyPayTypeCountEntity;
        ReportDailyPayTypeCountPK pk;
        PayTypeCountDTO payTypeCountDTO;
        for (Object[] queryResult : queryResultArray) {
            payTypeCountDTO = (PayTypeCountDTO) castNativeSelectResultToDTO(queryResult, PayTypeCountDTO.class);
            reportDailyPayTypeCountEntity = new ReportDailyPayTypeCountEntity();
            pk = new ReportDailyPayTypeCountPK(minusOneDay, payTypeCountDTO.getPayTypeId());
            reportDailyPayTypeCountEntity.setPk(pk);
            reportDailyPayTypeCountEntity.setCount(payTypeCountDTO.getCount());
            reportDailyPayTypeCountEntityList.add(reportDailyPayTypeCountEntity);
        }
        reportDailyPayTypeCountRepository.saveAll(reportDailyPayTypeCountEntityList);
    }

    private void buildDailyProductSalesReport(LocalDate minusOneDay, LocalDateTime todayStart, LocalDateTime todayEnd) {
        if (!reportDailyProductSalesRepository.findAllByPkDate(minusOneDay).isEmpty()) {
            return;
        }
        Object[][] queryResultArray = tradeRepository.getProductSalesByDateRange(todayStart, todayEnd);
        List<ReportDailyProductSalesEntity> reportDailyProductSalesEntityList = new ArrayList<>();
        ReportDailyProductSalesEntity reportDailyProductSalesEntity;
        ReportDailyProductSalesPK pk;
        ProductSalesDTO productSalesDTO;
        for (Object[] queryResult : queryResultArray) {
            productSalesDTO = (ProductSalesDTO) castNativeSelectResultToDTO(queryResult, ProductSalesDTO.class);
            reportDailyProductSalesEntity = new ReportDailyProductSalesEntity();
            pk = new ReportDailyProductSalesPK(minusOneDay, productSalesDTO.getProductId());
            reportDailyProductSalesEntity.setPk(pk);
            reportDailyProductSalesEntity.setCount(productSalesDTO.getCount());
            reportDailyProductSalesEntity.setTotalAmount(productSalesDTO.getTotalAmount());
            reportDailyProductSalesEntityList.add(reportDailyProductSalesEntity);
        }
        reportDailyProductSalesRepository.saveAll(reportDailyProductSalesEntityList);
    }

    private void buildDailyTradeStateReport(LocalDate minusOneDay, LocalDateTime todayStart, LocalDateTime todayEnd) {
        // 每日交易狀態     (日期, 交易狀態ID, 交易次數,交易總額)
        if (!reportDailyTradeStateRepository.findAllByPkDate(minusOneDay).isEmpty()) {
            return;
        }
        Object[][] queryResultArray = tradeRepository.getTradeStateByDateRange(todayStart, todayEnd);
        List<ReportDailyTradeStateEntity> reportDailyTradeStateEntityList = new ArrayList<>();
        ReportDailyTradeStateEntity reportDailyTradeStateEntity;
        ReportDailyTradeStatePK pk;
        TradeStateDTO tradeStateDTO;
        for (Object[] queryResult : queryResultArray) {
            tradeStateDTO = (TradeStateDTO) castNativeSelectResultToDTO(queryResult, TradeStateDTO.class);
            reportDailyTradeStateEntity = new ReportDailyTradeStateEntity();
            pk = new ReportDailyTradeStatePK(minusOneDay, tradeStateDTO.getTradeStateId());
            reportDailyTradeStateEntity.setPk(pk);
            reportDailyTradeStateEntity.setCount(tradeStateDTO.getCount());
            reportDailyTradeStateEntity.setTotalAmount(tradeStateDTO.getTotalAmount());
            reportDailyTradeStateEntity.setTotalAmountWithDiscount(tradeStateDTO.getTotalAmountWithDiscount());
            reportDailyTradeStateEntityList.add(reportDailyTradeStateEntity);
        }
        reportDailyTradeStateRepository.saveAll(reportDailyTradeStateEntityList);
    }


    private Object castNativeSelectResultToDTO(Object[] queryResultArray, Class<?> clazz) {
        Constructor<?> constructor;
        try {
            constructor = clazz.getConstructor(getClazzConstructorTypes(clazz));
            return constructor.newInstance(queryResultArray);
        } catch (NoSuchMethodException | InstantiationException | InvocationTargetException |
                 IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private Class<?>[] getClazzConstructorTypes(Class<?> clazz) {
        Field[] declaredFields = clazz.getDeclaredFields();
        Class<?>[] clazzFieldTypeArray = new Class[declaredFields.length];
        for (int i = 0; i < declaredFields.length; i++) {
            clazzFieldTypeArray[i] = declaredFields[i].getType();
        }
        return clazzFieldTypeArray;
    }

}
