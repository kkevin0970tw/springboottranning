package com.hengwei.side2.service;

import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.DoPayInput;
import com.hengwei.side2.vo.input.PurchaseInput;
import com.hengwei.side2.vo.input.TradeInfoInput;
import com.hengwei.side2.vo.output.DoPayOutput;
import com.hengwei.side2.vo.output.PurchaseOutput;
import com.hengwei.side2.vo.output.TradeInfoOutput;

public interface ShopService {
    APIResult<PurchaseOutput> purchase(PurchaseInput input);

    APIResult<TradeInfoOutput> tradeInfo(TradeInfoInput input);

    APIResult<DoPayOutput> doPay(DoPayInput input);
}
