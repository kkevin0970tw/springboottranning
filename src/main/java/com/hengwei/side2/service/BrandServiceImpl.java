package com.hengwei.side2.service;

import com.hengwei.side2.entity.BrandEntity;
import com.hengwei.side2.exception.Exceptions;
import com.hengwei.side2.exception.ExceptionsBuilder;
import com.hengwei.side2.repository.BrandRepository;
import com.hengwei.side2.repository.ProductRepository;
import com.hengwei.side2.utils.OptionalUtils;
import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.AddBrandInput;
import com.hengwei.side2.vo.input.DeleteBrandInput;
import com.hengwei.side2.vo.input.GetBrandByIdInput;
import com.hengwei.side2.vo.input.UpdateBrandInput;
import com.hengwei.side2.vo.output.AddBrandOutput;
import com.hengwei.side2.vo.output.GetAllBrandOutput;
import com.hengwei.side2.vo.output.GetBrandByIdOutput;
import com.hengwei.side2.vo.output.UpdateBrandOutput;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    private OptionalUtils<BrandEntity> optionalUtils;
    @Autowired
    private BrandRepository brandRepository;
    @Autowired
    private ProductRepository productRepository;

    @Override
    public APIResult<GetBrandByIdOutput> getBrandById(GetBrandByIdInput input) {
        BrandEntity brandEntity = optionalUtils.findOneOrThrow(
                input,
                brandRepository.findById(input.getId()),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );

        GetBrandByIdOutput output = new GetBrandByIdOutput(brandEntity);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetAllBrandOutput> getAllBrand() {
        List<BrandEntity> brandEntityList = optionalUtils.findMoreOrThrow(
                brandRepository.findAll(),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );

        List<GetAllBrandOutput.OutputData> outputDataList = new ArrayList<>();
        brandEntityList.forEach(entity -> {
            GetAllBrandOutput.OutputData data = new GetAllBrandOutput.OutputData(entity);
            outputDataList.add(data);
        });

        GetAllBrandOutput output = new GetAllBrandOutput(outputDataList);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<AddBrandOutput> addBrand(AddBrandInput input) {
        optionalUtils.existThenThrow(
                input,
                brandRepository.existsByNameENAndNameTW(input.getNameEN(), input.getNameTW()),
                Exceptions.DataBaseException.DATA_BASE_DATA_ALREADY_EXIST
        );

        BrandEntity brandEntity = new BrandEntity();
        BeanUtils.copyProperties(input, brandEntity);
        BrandEntity newBrandEntity = brandRepository.saveAndFlush(brandEntity);

        /* 檢視新增後的實體資料  非必要*/
        AddBrandOutput output = new AddBrandOutput();
        BeanUtils.copyProperties(newBrandEntity, output);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<UpdateBrandOutput> updateBrand(UpdateBrandInput input) {
        BrandEntity brandEntity = optionalUtils.findOneOrThrow(
                input,
                brandRepository.findById(input.getId()),
                Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
        );
        BeanUtils.copyProperties(input, brandEntity);
        BrandEntity updateBrandEntity = brandRepository.saveAndFlush(brandEntity);

        /* 檢視更新後的實體資料  非必要*/
        UpdateBrandOutput output = new UpdateBrandOutput();
        BeanUtils.copyProperties(updateBrandEntity, output);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<Void> deleteBrand(DeleteBrandInput input) {
        /*先確認品牌存在否*/
        BrandEntity brandEntity = brandRepository.findById(input.getId())
                .orElseThrow(() ->
                        ExceptionsBuilder.builder(Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST)
                                .setErrorLog(input.toString())
                                .build());
        /*再確認產品中有無使用到該品牌的資料*/
        if (productRepository.existsByBrandId(brandEntity.getId())) {
            throw ExceptionsBuilder.builder(Exceptions.DataBaseException.DATA_BASE_CONSTRAINT_FOREIGN_KEY)
                    .setErrorLog(input.toString())
                    .build();
        }
        brandRepository.delete(brandEntity);
        return new APIResult<>();
    }
}

