package com.hengwei.side2.service;

import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.AddBrandInput;
import com.hengwei.side2.vo.input.DeleteBrandInput;
import com.hengwei.side2.vo.input.GetBrandByIdInput;
import com.hengwei.side2.vo.input.UpdateBrandInput;
import com.hengwei.side2.vo.output.*;

public interface BrandService {
    APIResult<GetBrandByIdOutput> getBrandById(GetBrandByIdInput input);

    APIResult<GetAllBrandOutput> getAllBrand();

    APIResult<AddBrandOutput> addBrand(AddBrandInput input);

    APIResult<UpdateBrandOutput> updateBrand(UpdateBrandInput input);

    APIResult<Void> deleteBrand(DeleteBrandInput input);
}
