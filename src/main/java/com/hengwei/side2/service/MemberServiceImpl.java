package com.hengwei.side2.service;

import com.hengwei.side2.entity.MemberEntity;
import com.hengwei.side2.enumeration.UploadFilePathEnum;
import com.hengwei.side2.exception.Exceptions;
import com.hengwei.side2.exception.ExceptionsBuilder;
import com.hengwei.side2.repository.MemberRepository;
import com.hengwei.side2.utils.IOUtils;
import com.hengwei.side2.utils.OptionalUtils;
import com.hengwei.side2.utils.SecurityUtils;
import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.*;
import com.hengwei.side2.vo.output.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private OptionalUtils<MemberEntity> optionalUtils;
    @Autowired
    private IOUtils ioUtils;
    @Autowired
    private SecurityUtils securityUtils;
    @Autowired
    private MemberRepository memberRepository;


    @Override
    public APIResult<GetMemberByIdOutput> getMemberById(GetMemberByIdInput input) {
        MemberEntity memberEntity = optionalUtils.findOneOrThrow(
                input,
                memberRepository.findById(input.getId()),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );

        GetMemberByIdOutput output = new GetMemberByIdOutput(memberEntity);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetMembersByRoeIdOutput> getMembersByRoleId(GetMembersByRoleIdInput input) {
        List<MemberEntity> memberEntityList = optionalUtils.findMoreOrThrow(
                input,
                memberRepository.findAllByRoleId(input.getRoleId()),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );

        List<GetMembersByRoeIdOutput.OutputData> outputDataList = new ArrayList<>();
        memberEntityList.forEach(entity -> {
            GetMembersByRoeIdOutput.OutputData data = new GetMembersByRoeIdOutput.OutputData(entity);
            outputDataList.add(data);
        });

        GetMembersByRoeIdOutput output = new GetMembersByRoeIdOutput(outputDataList);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<GetAllMembersOutput> getAllMembers() {
        List<MemberEntity> memberEntityList = optionalUtils.findMoreOrThrow(
                memberRepository.findAll(),
                Exceptions.ResultException.RESULT_NOT_FOUND
        );

        List<GetAllMembersOutput.OutputData> outputDataList = new ArrayList<>();
        memberEntityList.forEach(entity -> {
            GetAllMembersOutput.OutputData data = new GetAllMembersOutput.OutputData(entity);
            outputDataList.add(data);
        });

        GetAllMembersOutput output = new GetAllMembersOutput(outputDataList);
        return new APIResult<>(output);
    }

    @Override
    @Transactional
    public APIResult<AddMemberOutput> addMember(AddMemberInput input) {
        optionalUtils.existThenThrow(
                input,
                memberRepository.findByAccount(input.getAccount()),
                Exceptions.DataBaseException.DATA_BASE_DATA_ALREADY_EXIST
        );

        MemberEntity memberEntity = new MemberEntity();
        BeanUtils.copyProperties(input, memberEntity);
        ioUtils.saveImgFile(UploadFilePathEnum.PROFILE_IMG_PATH.getPath(), input.getImgFile(), input.getAccount());
        memberEntity.setPassword(securityUtils.encodePassword(input.getPassword()));
        MemberEntity newMemberEntity = memberRepository.saveAndFlush(memberEntity);

        /* 檢視更新後的實體資料  非必要*/
        AddMemberOutput output = new AddMemberOutput();
        BeanUtils.copyProperties(newMemberEntity, output);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<DeleteMemberOutput> deleteMember(DeleteMemberInput input) {
        /* 預計建立一個新的歷史會員表  將刪除的會員移動到 歷史會員表*/
        return null;
    }

    @Override
    public APIResult<UpdateMemberOutput> updateMember(UpdateMemberInput input) {
        MemberEntity memberEntity = optionalUtils.findOneOrThrow(
                input,
                memberRepository.findById(input.getId()),
                Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
        );

        validAccountDiff(input.getAccount(), memberEntity.getAccount());

        BeanUtils.copyProperties(input, memberEntity);
        MemberEntity newMemberEntity = memberRepository.saveAndFlush(memberEntity);

        /* 檢視更新後的實體資料  非必要*/
        UpdateMemberOutput output = new UpdateMemberOutput();
        BeanUtils.copyProperties(newMemberEntity, output);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<ResetPasswordOutput> resetPassword(ResetPasswordInput input) {
        validPassword(input);
        MemberEntity memberEntity = optionalUtils.findOneOrThrow(
                input,
                memberRepository.findByIdAndAccount(input.getId(), input.getAccount()),
                Exceptions.AccountException.ACCOUNT_NOT_EXIST
        );

        validOldPassword(input, memberEntity);
        memberEntity.setPassword(passwordEncoder.encode(input.getNewPassword()));
        MemberEntity newPasswordMemberEntity = memberRepository.saveAndFlush(memberEntity);

        /* 檢視更新後的實體資料  非必要*/
        ResetPasswordOutput output = new ResetPasswordOutput();
        BeanUtils.copyProperties(newPasswordMemberEntity, output);
        return new APIResult<>();
    }

    @Override
    public APIResult<MemberLoginOutput> memberLogin(MemberLoginInput input) {
        MemberEntity memberEntity = optionalUtils.findOneOrThrow(
                input,
                memberRepository.findByAccountAndPassword(input.getAccount(), input.getPassword()),
                Exceptions.AccountException.ACCOUNT_NOT_EXIST
        );

        memberEntity.setLastLogin(LocalDateTime.now());
        return new APIResult<>();
    }

    private void validAccountDiff(String inputAccount, String entityAccount) {
        if (inputAccount.equals(entityAccount)) {
            return;
        }
        throw ExceptionsBuilder.builder(Exceptions.AccountException.ACCOUNT_NOT_EXIST).build();
    }

    private void validPassword(ResetPasswordInput input) {
        if (input.getOldPassword().equals(input.getNewPassword())) {
            throw ExceptionsBuilder.builder(Exceptions.ParamException.PARAM_EXCEPTION)
                    .setErrorLog("新舊密碼不得相同")
                    .build();
        }
    }

    private void validOldPassword(ResetPasswordInput input, MemberEntity memberEntity) {
        if (!passwordEncoder.matches(input.getOldPassword(), memberEntity.getPassword())) {
            throw ExceptionsBuilder.builder(Exceptions.AccountException.PASSWORD_NOT_EQUAL).build();
        }
    }
}
