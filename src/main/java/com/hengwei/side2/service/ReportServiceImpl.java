package com.hengwei.side2.service;

import com.hengwei.side2.entity.*;
import com.hengwei.side2.repository.ProductRepository;
import com.hengwei.side2.repository.ReportDailyProductSalesRepository;
import com.hengwei.side2.utils.CacheUtils;
import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.GenerateMemberReportInput;
import com.hengwei.side2.vo.input.GenerateProductSalesReportInput;
import com.hengwei.side2.vo.input.GenerateTradeReportInput;
import com.hengwei.side2.vo.output.GenerateMemberReportOutput;
import com.hengwei.side2.vo.output.GenerateProductSalesReportOutput;
import com.hengwei.side2.vo.output.GenerateTradeReportOutput;
import jakarta.persistence.criteria.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ReportDailyProductSalesRepository reportDailyProductSalesRepository;
    @Autowired
    private CacheUtils<CategoryEntity> categoryCacheUtils;
    @Autowired
    private CacheUtils<WineSubTypeEntity> wineSubTypeCacheUtils;
    @Autowired
    private CacheUtils<CountryEntity> countryCacheUtils;
    @Autowired
    private CacheUtils<AreaEntity> areaCacheUtils;
    @Autowired
    private CacheUtils<BrandEntity> brandCacheUtils;
    @Autowired
    private CacheUtils<GrapeVarietiesEntity> grapeVarietiesCacheUtils;
    @Autowired
    private CacheUtils<OakEntity> oakCacheUtils;
    @Autowired
    private CacheUtils<ProductEntity> productCacheUtils;

    @Override
    public APIResult<GenerateMemberReportOutput> generateMemberReport(GenerateMemberReportInput input) {
        return null;
    }


    @Override
    public APIResult<GenerateProductSalesReportOutput> generateProductReport(GenerateProductSalesReportInput input) {
        Specification<ReportDailyProductSalesEntity> spec = (root, query, builder) -> {
            Predicate date = builder.between(root.get("pk").get("date"), input.getStarDate(), input.getEndDate());
            Predicate productId = root.get("pk").get("productId").in((Object[]) input.getProductIdArray());
            query.orderBy(builder.asc(root.get("pk").get("date")));
            return builder.and(date, productId);
        };

        List<ReportDailyProductSalesEntity> all = reportDailyProductSalesRepository.findAll(spec);
        GenerateProductSalesReportOutput output = new GenerateProductSalesReportOutput();
        output.setOutPutDataList(new ArrayList<>());

        all.forEach(reportDailyProductSalesEntity -> {
            GenerateProductSalesReportOutput.OutputData data = new GenerateProductSalesReportOutput.OutputData();
            data.setDate(reportDailyProductSalesEntity.getPk().getDate());
            data.setProductName(productCacheUtils.getProductNameFromCache2(productRepository, reportDailyProductSalesEntity.getPk().getProductId().intValue(), String.class));
            data.setCount(reportDailyProductSalesEntity.getCount());
            data.setTotalAmount(reportDailyProductSalesEntity.getTotalAmount());
            output.getOutPutDataList().add(data);
        });

        return new APIResult<>(output);
    }

    @Override
    public APIResult<GenerateTradeReportOutput> generateTradeReport(GenerateTradeReportInput input) {
        return null;

    }

}

