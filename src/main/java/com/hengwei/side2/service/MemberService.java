package com.hengwei.side2.service;

import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.*;
import com.hengwei.side2.vo.output.*;

public interface MemberService {

    APIResult<GetMemberByIdOutput> getMemberById(GetMemberByIdInput input);

    APIResult<GetMembersByRoeIdOutput> getMembersByRoleId(GetMembersByRoleIdInput input);

    APIResult<GetAllMembersOutput> getAllMembers();

    APIResult<AddMemberOutput> addMember(AddMemberInput input);

    APIResult<DeleteMemberOutput> deleteMember(DeleteMemberInput input);

    APIResult<UpdateMemberOutput> updateMember(UpdateMemberInput input);

    APIResult<ResetPasswordOutput> resetPassword(ResetPasswordInput input);

    APIResult<MemberLoginOutput> memberLogin(MemberLoginInput input);

}
