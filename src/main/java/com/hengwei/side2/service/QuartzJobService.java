package com.hengwei.side2.service;

import org.springframework.stereotype.Service;

public interface QuartzJobService {

    //自動購買產生 ( 產生 交易紀錄 及 交易明細 )
    void autoBuy();

    //自動固定時間產生 每日各式報表
    void dailyReportBuild();


}
