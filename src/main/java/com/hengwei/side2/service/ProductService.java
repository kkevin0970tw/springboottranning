package com.hengwei.side2.service;

import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.*;
import com.hengwei.side2.vo.output.*;

public interface ProductService {
    APIResult<GetProductByIdOutput> getProductById(GetProductByIdInput input);

    APIResult<GetProductsWithPageOutput> getProductsWithPage(GetProductsWithPageInput input);

    APIResult<GetProductsByCategoryWithPageOutput> getProductsByCategoryWithPage(GetProductsByCategoryWithPageInput input);

    APIResult<GetProductsByWineSubTypeWithPageOutput> getProductsByWineSubTypeWithPage(GetProductsByWineSubTypeInputWithPage input);

    APIResult<GetProductsByCountryWithPageOutput> getProductsByCountryWithPage(GetProductsByCountryWithPageInput input);

    APIResult<GetProductsByAreaWithPageOutput> getProductsByAreaWithPage(GetProductsByAreaWithPageInput input);

    APIResult<GetProductsByBrandWithPageOutput> getProductsByBrandWithPage(GetProductsByBrandWithPageInput input);

    APIResult<GetProductsByGrapeVarietiesWithPageOutput> getProductsByGrapeVarietiesWithPage(GetProductsByGrapeVarietiesWithPageInput input);

    APIResult<GetProductsByOakWithPageOutput> getProductsByOakWithPage(GetProductsByOakWithPageInput input);

    APIResult<GetProductsByPriceRangeWithPageOutput> getProductsByPriceRangeWithPage(GetProductsByPriceRangeWithPageInput input);

    APIResult<UpdateProductOutput> updateProduct(UpdateProductInput input);

    APIResult<DeleteProductOutput> deleteProduct(DeleteProductInput input);

    APIResult<AddProductOutput> addProduct(AddProductInput input);
}
