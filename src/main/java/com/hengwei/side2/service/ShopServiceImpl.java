package com.hengwei.side2.service;

import com.hengwei.side2.entity.*;
import com.hengwei.side2.enumeration.PayTypeEnum;
import com.hengwei.side2.enumeration.TradeStateEnum;
import com.hengwei.side2.exception.Exceptions;
import com.hengwei.side2.exception.ExceptionsBuilder;
import com.hengwei.side2.repository.*;
import com.hengwei.side2.utils.CacheUtils;
import com.hengwei.side2.utils.OptionalUtils;
import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.DoPayInput;
import com.hengwei.side2.vo.input.PurchaseInput;
import com.hengwei.side2.vo.input.TradeInfoInput;
import com.hengwei.side2.vo.output.DoPayOutput;
import com.hengwei.side2.vo.output.PurchaseOutput;
import com.hengwei.side2.vo.output.TradeInfoOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ShopServiceImpl implements ShopService {
    @Autowired
    private CacheUtils<TradeStateEntity> tradeStateCacheUtils;
    @Autowired
    private CacheUtils<PayTypeEntity> payTypeCacheUtils;
    @Autowired
    private OptionalUtils<MemberEntity> memberOptionalUtils;
    @Autowired
    private OptionalUtils<ProductEntity> productOptionalUtils;
    @Autowired
    private OptionalUtils<TradeEntity> tradeOptionalUtils;
    @Autowired
    private OptionalUtils<TradeDetailEntity> tradeDetailOptionalUtils;

    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private TradeRepository tradeRepository;
    @Autowired
    private TradeDetailRepository tradeDetailRepository;
    @Autowired
    private TradeStateRepository tradeStateRepository;
    @Autowired
    private PayTypeRepository payTypeRepository;

    @Override
    @Transactional
    public APIResult<PurchaseOutput> purchase(PurchaseInput input) {
        checkMemberExist(input.getMemberId());
        checkPurchaseListNotDuplicate(input.getPurchaseItemList());

        /* 先完成 交易紀錄(主)*/
        TradeEntity tradeEntity = saveTrade(input);
        /* 後完成 交易紀錄細項(副)*/
        List<TradeDetailEntity> tradeDetailEntityList = saveTradeDetail(tradeEntity, input.getPurchaseItemList());
//        doPay(tradeEntity);

        tradeRepository.flush();
        tradeDetailRepository.flush();

        PurchaseOutput output = new PurchaseOutput();
        output.setTrade(tradeEntity);
        output.setTradeDetailList(tradeDetailEntityList);
        return new APIResult<>(output);
    }

    @Override
    public APIResult<TradeInfoOutput> tradeInfo(TradeInfoInput input) {
        TradeInfoOutput output = new TradeInfoOutput();
        TradeEntity tradeEntity = tradeOptionalUtils.findOneOrThrow(
                input,
                tradeRepository.findByIdAndTradeDateTimeBetween
                        (
                                input.getTradeId(),
                                LocalDateTime.of(input.getBeginDate(), LocalTime.MIN),
                                LocalDateTime.of(input.getEndDate(), LocalTime.MAX)
                        ),
                Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
        );
        output.setTrade(processTradeEntityToOutput(tradeEntity));

        if (input.isIncludeDetail()) {
            TradeDetailPK pk = new TradeDetailPK();
            pk.setTradeId(input.getTradeId());
            List<TradeDetailEntity> tradeDetailEntityList = tradeDetailOptionalUtils.findMoreOrThrow(
                    input,
                    tradeDetailRepository.findAllByPkTradeId(input.getTradeId()),
                    Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
            );
            output.setTradeDetailList(processTradeDetailListToOutput(tradeDetailEntityList));
        }
        return new APIResult<>(output);
    }

    @Override
    @Transactional
    public APIResult<DoPayOutput> doPay(DoPayInput input) {
        /* 先確認訂單是否存在*/
        TradeEntity tradeEntity = tradeOptionalUtils.findOneOrThrow(
                input,
                tradeRepository.findById(input.getTradeId()),
                Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
        );

        /* 確認 金額/交易方式 是否匹配*/
        checkPayAmountAndPayType(input, tradeEntity);
        /* 檢查授權*/
//        checkAuthorization(input);
        doPay(tradeEntity);
        return new APIResult<>();
    }

    private void checkPayAmountAndPayType(DoPayInput input, TradeEntity tradeEntity) {
        if (input.getPayTypeId().equals(tradeEntity.getPayTypeId()) &&
                input.getPayAmount().equals(tradeEntity.getTradeAmountWithDiscount())) {
            /* Do Nothing */
            return;
        }
        throw ExceptionsBuilder.builder(Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST).build();
    }

    private TradeInfoOutput.Trade processTradeEntityToOutput(TradeEntity tradeEntity) {
        TradeInfoOutput.Trade trade = new TradeInfoOutput.Trade();
        trade.setTradeId(tradeEntity.getId());
        trade.setMemberId(tradeEntity.getMemberId());
        trade.setTradeAmountWithDiscount(tradeEntity.getTradeAmountWithDiscount());
        trade.setTradeAmount(tradeEntity.getTradeAmount());
        trade.setTradeDateTime(tradeEntity.getTradeDateTime());
        trade.setPayType(payTypeCacheUtils.getDataFromCache2(payTypeRepository, tradeEntity.getPayTypeId(), PayTypeEntity.class).getNameTW());
        trade.setTradeState(tradeStateCacheUtils.getDataFromCache2(tradeStateRepository, tradeEntity.getTradeStateId(), TradeStateEntity.class).getStateEN());
        return trade;
    }

    private List<TradeInfoOutput.TradeDetail> processTradeDetailListToOutput(List<TradeDetailEntity> tradeDetailEntityList) {
        List<TradeInfoOutput.TradeDetail> tradeDetailList = new ArrayList<>();
        tradeDetailEntityList.forEach(tradeDetailItem -> {
            TradeInfoOutput.TradeDetail tradeDetail = new TradeInfoOutput.TradeDetail();
            tradeDetail.setProductId(tradeDetailItem.getPk().getProductId());
            tradeDetail.setPrice(tradeDetailItem.getPrice());
            tradeDetail.setDiscount(tradeDetailItem.getDiscount());
            tradeDetail.setQuantity(tradeDetailItem.getQuantity());
            tradeDetailList.add(tradeDetail);
        });
        return tradeDetailList;
    }

    private void checkPurchaseListNotDuplicate(List<PurchaseInput.PurchaseItem> purchaseItemList) {
        Set<Integer> set = new HashSet<>();
        for (PurchaseInput.PurchaseItem item : purchaseItemList) {
            if (!set.add(item.getProductId())) {
                throw ExceptionsBuilder.builder(Exceptions.ParamException.PARAM_EXCEPTION)
                        .setErrorLog(purchaseItemList)
                        .setCustomMessage("購買產品重複")
                        .build();
            }
        }
    }

    private void checkMemberExist(Integer memberId) {
        memberOptionalUtils.notExistThenThrow(
                memberId,
                memberRepository.findById(memberId),
                Exceptions.AccountException.ACCOUNT_NOT_EXIST
        );
    }

    private TradeEntity saveTrade(PurchaseInput input) {
        TradeEntity tradeEntity = new TradeEntity();
        tradeEntity.setTradeDateTime(LocalDateTime.now());
        tradeEntity.setTradeStateId(TradeStateEnum.UNPAID.getStateNum());
        tradeEntity.setMemberId(input.getMemberId());
        tradeEntity.setTradeAmount(input.getPurchaseItemList().stream().mapToInt(item -> (int) item.getPrice() * item.getQuantity()).sum());
        tradeEntity.setTradeAmountWithDiscount(input.getPurchaseItemList().stream().mapToInt(item -> (int) (item.getPrice() * item.getQuantity() * item.getDiscount() * 0.01)).sum());
        tradeEntity.setPayTypeId(input.getPayTypeId());
        return tradeRepository.save(tradeEntity);
    }

    private List<TradeDetailEntity> saveTradeDetail(TradeEntity tradeEntity, List<PurchaseInput.PurchaseItem> purchaseItemList) {
        List<TradeDetailEntity> tradeDetailEntityList = new ArrayList<>();
        for (PurchaseInput.PurchaseItem item : purchaseItemList) {
            /*避免時間差 前端使用者尚未更新畫面及售價 導致與資料庫實際售價不同產生資料錯誤*/
            checkProductDataConsistent(item);
            TradeDetailEntity tradeDetailEntity = new TradeDetailEntity();
            tradeDetailEntity.setPk(new TradeDetailPK(tradeEntity.getId(), item.getProductId()));
            tradeDetailEntity.setPrice(item.getPrice());
            tradeDetailEntity.setDiscount(item.getDiscount());
            tradeDetailEntity.setQuantity(item.getQuantity());
            tradeDetailEntityList.add(tradeDetailEntity);
        }
        return tradeDetailRepository.saveAll(tradeDetailEntityList);
    }

    private void checkProductDataConsistent(PurchaseInput.PurchaseItem item) {
        ProductEntity productEntity = productOptionalUtils.findOneOrThrow(
                item,
                productRepository.findById(item.getProductId()),
                Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
        );
        if (productEntity.getDiscount().equals(item.getDiscount()) && productEntity.getPrice().equals(item.getPrice())) {
            // Do Nothing
        } else {
            throw ExceptionsBuilder.builder(Exceptions.DataBaseException.DATA_BASE_CONSISTENT_ERROR)
                    .setErrorLog(item, productRepository)
                    .setCustomMessage("產品資料出現不一致狀態，導致交易失敗，請通知客服")
                    .build();
        }
    }

    private void doPay(TradeEntity tradeEntity) {
        PayTypeEntity payTypeEntity = payTypeCacheUtils.getDataFromCache2(
                payTypeRepository,
                tradeEntity.getPayTypeId(),
                PayTypeEntity.class
        );
        PayTypeEnum payTypeEnum = PayTypeEnum.valueOf(payTypeEntity.getKeyName());
        int payAmount = tradeEntity.getTradeAmount();
        switch (payTypeEnum) {
            case CASH, BANK_TRANSFER, CREDIT_CARD, LINE_PAY, ALI_PAY -> {
                tradeEntity.setTradeStateId(TradeStateEnum.PAID.getStateNum());
                tradeEntity.setPayDateTime(LocalDateTime.now());
            }
            case CASH_ON_DELIVERY -> tradeEntity.setTradeStateId(TradeStateEnum.PENDING_SHIPMENT.getStateNum());
        }

        /* 邏輯上應為  對應交易方式 各自處理，交易失敗則狀態更改為 未付款 ， 否則改為已付款，透過發貨API 更改狀態為發貨  暫定 暫不實作*/
    }
}

