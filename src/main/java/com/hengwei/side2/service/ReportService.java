package com.hengwei.side2.service;

import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.GenerateMemberReportInput;
import com.hengwei.side2.vo.input.GenerateProductSalesReportInput;
import com.hengwei.side2.vo.input.GenerateTradeReportInput;
import com.hengwei.side2.vo.output.GenerateMemberReportOutput;
import com.hengwei.side2.vo.output.GenerateProductSalesReportOutput;
import com.hengwei.side2.vo.output.GenerateTradeReportOutput;

public interface ReportService {
    APIResult<GenerateMemberReportOutput> generateMemberReport(GenerateMemberReportInput input);

    APIResult<GenerateProductSalesReportOutput> generateProductReport(GenerateProductSalesReportInput input);

    APIResult<GenerateTradeReportOutput> generateTradeReport(GenerateTradeReportInput input);
}
