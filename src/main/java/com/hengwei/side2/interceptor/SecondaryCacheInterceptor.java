package com.hengwei.side2.interceptor;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.CacheInterceptor;
import org.springframework.data.redis.cache.RedisCache;

public class SecondaryCacheInterceptor extends CacheInterceptor {
    private final CacheManager cacheManager;

    public SecondaryCacheInterceptor(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    // 攔截 透過Cache 取資料的動作
    // 用於 若有透過 Redis取資料時，必須往Caffeine 回存
    // 影響 下次即可直接透郭Caffeine 取  並減少 Redis 存取
    @Override
    protected Cache.ValueWrapper doGet(Cache cache, Object key) {
        logger.info(String.format("Cache Class : {%s} \tCache Name :{%s} \t  key :{%s}", cache.getClass().getSimpleName(), cache.getName(), key.toString()));
        Cache.ValueWrapper valueWrapper = super.doGet(cache, key);
        if (valueWrapper != null && cache.getClass() == RedisCache.class) {
            cacheManager.getCache(cache.getName()).putIfAbsent(key, valueWrapper.get());  // 因@Primary 已設定於 CaffeineCache上 ，故 cacheManager 取的會是 CaffeineCache
        }
        return valueWrapper;
    }
}
