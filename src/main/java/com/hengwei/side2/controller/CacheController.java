package com.hengwei.side2.controller;

import com.github.benmanes.caffeine.cache.stats.CacheStats;
import com.hengwei.side2.vo.common.APIResult;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@RestController("/cache")
public class CacheController {


    @Autowired
    private CacheManager cacheManager;

    @GetMapping("/status")
    public APIResult<Map<String, Object>> getStatus(
            @Schema(defaultValue = "cache1", allowableValues = {"cache1", "cache2", "cache3"})
            @RequestParam
            String cacheName) {
        CaffeineCache cache = (CaffeineCache) cacheManager.getCache(cacheName);
        CacheStats stats = cache.getNativeCache().stats();
        Map<String, Object> map = new HashMap<>(9);
        map.put("Request Count", stats.requestCount());
        map.put("Hit Count", stats.hitCount());
        map.put("Miss Count", stats.missCount());
        map.put("Load Success Count", stats.loadSuccessCount());
        map.put("Load Fail Count", stats.loadFailureCount());
        map.put("Load Fail Rate", stats.loadFailureRate());
        map.put("Total Load Time ", stats.totalLoadTime());
        map.put("Eviction Count", stats.evictionCount());
        map.put("Eviction Weight", stats.evictionWeight());
        return new APIResult<>(map);
    }


    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @GetMapping("/show")
    public APIResult<ConcurrentMap<Object, Object>> show(
            @Schema(defaultValue = "cache1", allowableValues = {"cache1", "cache2", "cache3"})
            @RequestParam
            String cacheName,
            @Schema(defaultValue = "redis", allowableValues = {"redis", "caffeine"})
            @RequestParam
            String cacheType
    ) {
        if (cacheType.equals("redis")) {
            ConcurrentMap<Object, Object> map = new ConcurrentHashMap<>();
            Set<String> keys = redisTemplate.keys(cacheName.concat("*"));
            for (Object key : keys) {
                map.put(key, redisTemplate.opsForValue().get(key));
            }
            return new APIResult<>(map);

        } else {
            CaffeineCache cache = (CaffeineCache) cacheManager.getCache(cacheName);
//            assert cache != null;
            return new APIResult<>(cache.getNativeCache().asMap());
        }
    }

    @GetMapping("/clear")
    public APIResult<Boolean> clear(
            @Schema(defaultValue = "cache1", allowableValues = {"cache1", "cache2", "cache3"})
            @RequestParam
            String cacheName,
            @Schema(defaultValue = "redis", allowableValues = {"redis", "caffeine"})
            @RequestParam
            String cacheType
    ) {
        if (cacheType.equals("redis")) {
            Set<String> keys = redisTemplate.keys(cacheName.concat("*"));
            redisTemplate.delete(keys);
        } else {
            CaffeineCache cache = (CaffeineCache) cacheManager.getCache(cacheName);
            cache.clear();
        }
        return new APIResult<>();
    }
}
