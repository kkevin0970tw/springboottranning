package com.hengwei.side2.controller;

import com.hengwei.side2.service.BrandService;
import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.AddBrandInput;
import com.hengwei.side2.vo.input.DeleteBrandInput;
import com.hengwei.side2.vo.input.GetBrandByIdInput;
import com.hengwei.side2.vo.input.UpdateBrandInput;
import com.hengwei.side2.vo.output.AddBrandOutput;
import com.hengwei.side2.vo.output.GetAllBrandOutput;
import com.hengwei.side2.vo.output.GetBrandByIdOutput;
import com.hengwei.side2.vo.output.UpdateBrandOutput;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/brand")
@ApiResponse(description = "Response Code [ -1:失敗, 0:未知, 1:成功]", content = @Content(mediaType = "application/json"))
@Tag(name = "Brand API", description = " 品牌相關 API  ( 增 / 改 / 刪 / 查) ")
public class BrandController {
    @Autowired
    private BrandService brandService;

    @Operation(summary = "查詢品牌(一筆)")
    @GetMapping("")
    public APIResult<GetBrandByIdOutput> getBrandById(@Validated GetBrandByIdInput input) {
        return brandService.getBrandById(input);
    }

    @Operation(summary = "查詢品牌(全部)")
    @GetMapping("/all")
    public APIResult<GetAllBrandOutput> getAllBrand() {
        return brandService.getAllBrand();
    }

    @Operation(summary = "新增品牌(一筆)")
    @PostMapping("/add")
    public APIResult<AddBrandOutput> addBrand(@RequestBody @Validated AddBrandInput input) {
        return brandService.addBrand(input);
    }

    @Operation(summary = "更新品牌(一筆)")
    @PostMapping("/update")
    public APIResult<UpdateBrandOutput> updateBrand(@RequestBody @Validated UpdateBrandInput input) {
        return brandService.updateBrand(input);
    }

    @Operation(summary = "刪除品牌(一筆)")
    @PostMapping("/delete")
    public APIResult<Void> deleteBrand(@RequestBody @Validated DeleteBrandInput input) {
        return brandService.deleteBrand(input);
    }
}
