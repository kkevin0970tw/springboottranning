package com.hengwei.side2.controller;

import com.hengwei.side2.service.MemberService;
import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.*;
import com.hengwei.side2.vo.output.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/member")
@ApiResponse(description = "Response Code [ -1:失敗, 0:未知, 1:成功]", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
@Tag(name = "Member API", description = " 會員相關 API  ( 增 / 改 / 刪 / 查) ")
public class MemberController {

    @Autowired
    private MemberService memberService;

    @Operation(summary = "查詢會員(一筆)")
    @GetMapping("")
    public APIResult<GetMemberByIdOutput> getMemberById(@Validated GetMemberByIdInput input) {
        return memberService.getMemberById(input);
    }

    @Operation(summary = "查詢會員 by 會員等級(多筆)")
    @GetMapping("/role")
    public APIResult<GetMembersByRoeIdOutput> getMembersByRoleId(@Validated GetMembersByRoleIdInput input) {
        return memberService.getMembersByRoleId(input);
    }

    @Operation(summary = "查詢會員(全部)")
    @GetMapping("/all")
    public APIResult<GetAllMembersOutput> getAllMembers() {
        return memberService.getAllMembers();
    }

    @Operation(summary = "新增會員(一筆)")
    @PostMapping("/add")
    public APIResult<AddMemberOutput> addMember(@Validated AddMemberInput input) {
        return memberService.addMember(input);
    }

    @Operation(summary = "刪除會員(一筆)")
    @PostMapping("/delete")
    public APIResult<DeleteMemberOutput> deleteMember(@RequestBody @Validated DeleteMemberInput input) {
        return memberService.deleteMember(input);
    }

    @Operation(summary = "更新會員(一筆)")
    @PostMapping("/update")
    public APIResult<UpdateMemberOutput> updateMember(@RequestBody @Validated UpdateMemberInput input) {
        return memberService.updateMember(input);
    }

    @Operation(summary = "密碼重設")
    @PostMapping("/resetPassword")
    public APIResult<ResetPasswordOutput> resetPassword(@RequestBody @Validated ResetPasswordInput input) {
        return memberService.resetPassword(input);
    }


    @Operation(summary = "會員登入測試頁面")
    @GetMapping("/login")
    public String memberLoginPage (){
        return "MemberLoginTestPage";
    }
    @Operation(summary = "會員登入")
    @PostMapping("/login")
    public APIResult<MemberLoginOutput> memberLogin (@RequestBody @Validated MemberLoginInput input){
        return memberService.memberLogin(input);
    }
}
