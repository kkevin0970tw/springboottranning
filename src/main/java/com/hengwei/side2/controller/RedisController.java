package com.hengwei.side2.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hengwei.side2.entity.MemberEntity;
import com.hengwei.side2.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/redis")
public class RedisController {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private MemberRepository memberRepository;


//    @GetMapping("/count")
//    public String count() {
//        return STR."已經訪問\{stringRedisTemplate.opsForValue().increment("count")}次";
//    }

    @GetMapping("/string")
    public String string() {
        stringRedisTemplate.opsForValue().set("SetV1", UUID.randomUUID().toString());
        return stringRedisTemplate.opsForValue().get("SetV1");
    }

    @GetMapping("/list")
    public String setAndGet() {
        stringRedisTemplate.opsForList().leftPush("list1", UUID.randomUUID().toString());
        stringRedisTemplate.opsForList().leftPush("list1", UUID.randomUUID().toString());
        stringRedisTemplate.opsForList().leftPush("list1", UUID.randomUUID().toString());
        return stringRedisTemplate.opsForList().leftPop("list1");
    }

    @GetMapping("/setAndGetObject")
    public MemberEntity setAndGetObject(Integer id) throws JsonProcessingException {
        MemberEntity memberEntity = memberRepository.getReferenceById(id);
        redisTemplate.opsForValue().set(memberEntity.getAccount(), new ObjectMapper().writeValueAsString(memberEntity));
        return (MemberEntity) redisTemplate.opsForValue().get(memberEntity.getAccount());
    }
}
