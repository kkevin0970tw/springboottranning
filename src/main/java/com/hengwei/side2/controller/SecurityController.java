package com.hengwei.side2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/security")
public class SecurityController {


    @GetMapping("/form-login/login")
    public String getLoginPage() {
        return "login";
    }

    @PostMapping("/error")
    public String errorPage() {
        return "login";
    }

    @GetMapping("/page403")
    public String get403() {
        return "page403";
    }

    @PostMapping("/form-login/authentication-success")
    public String authenticationSuccess(Model model) {
        return returnAuthTestAndSetAttribute(model, "手動開發者", "手動開發者");
    }

    @PostMapping("/form-login/authentication-fail")
    public String authenticationFail() {
        return "authorizationTestPage/authentication-fail";
    }


    /*  測試用*/
    /*  用於登入後按照權限跳轉至 對應頁面*/
    @GetMapping("/auto-direct/user1")
    public String user1() {
        return getAutoDirectPage("user1");
    }

    @GetMapping("/auto-direct/user2")
    public String user2() {
        return getAutoDirectPage("user2");
    }

    @GetMapping("/auto-direct/user3")
    public String user3() {
        return getAutoDirectPage("user3");
    }

    @GetMapping("/auto-direct/user4")
    public String user4() {
        return getAutoDirectPage("user4");
    }

    @GetMapping("/auto-direct/user5")
    public String user5() {
        return getAutoDirectPage("user5");
    }

    @GetMapping("/auto-direct/admin")
    public String admin() {
        return getAutoDirectPage("admin");
    }

    @GetMapping("/auto-direct/root")
    public String root() {
        return getAutoDirectPage("root");
    }

    private String getAutoDirectPage(String page) {
        return "afterLoginAutoDirectPage/".concat(page);
    }


    /*  測試用*/
    /*  用於登入後測試權限是否足夠 取得對應訊息 */
    @GetMapping("/user1/security-auth-test")
    public String user1AuthTest(Model model, String username, String role) {
        return returnAuthTestAndSetAttribute(model, username, role);
    }

    @GetMapping("/user2/security-auth-test")
    public String user2AuthTest(Model model, String username, String role) {
        return returnAuthTestAndSetAttribute(model, username, role);
    }

    @GetMapping("/user3/security-auth-test")
    public String user3AuthTest(Model model, String username, String role) {
        return returnAuthTestAndSetAttribute(model, username, role);
    }

    @GetMapping("/user4/security-auth-test")
    public String user4AuthTest(Model model, String username, String role) {
        return returnAuthTestAndSetAttribute(model, username, role);
    }

    @GetMapping("/user5/security-auth-test")
    public String user5AuthTest(Model model, String username, String role) {
        return returnAuthTestAndSetAttribute(model, username, role);
    }

    @GetMapping("/root/security-auth-test")
    public String rootAuthTest(Model model, String username, String role) {
        return returnAuthTestAndSetAttribute(model, username, role);
    }

    @GetMapping("/admin/security-auth-test")
    public String adminAuthTest(Model model, String username, String role) {
        return returnAuthTestAndSetAttribute(model, username, role);
    }

    public String returnAuthTestAndSetAttribute(Model model, String username, String role) {
        model.addAttribute("username", username);
        model.addAttribute("role", role);
        return "authorizationTestPage/authentication-success";
    }
}
