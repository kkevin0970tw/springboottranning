package com.hengwei.side2.controller;

import com.hengwei.side2.service.ProductService;
import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.*;
import com.hengwei.side2.vo.output.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
@ApiResponse(description = "Response Code [ -1:失敗, 0:未知, 1:成功]", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
@Tag(name = "Product API", description = " 產品相關 API  ( 增 / 改 / 刪 / 查) ")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Operation(summary = "查詢產品(一筆)")
    @GetMapping("")
    public APIResult<GetProductByIdOutput> getProductById(@Validated GetProductByIdInput input) {
        return productService.getProductById(input);
    }

    @Operation(summary = "查詢產品並帶分頁(多筆)")
    @GetMapping("/all")
    public APIResult<GetProductsWithPageOutput> getProductsWithPage(@Validated GetProductsWithPageInput input) {
        return productService.getProductsWithPage(input);
    }

    @Operation(summary = "查詢產品透過種類並帶分頁(多筆)")
    @GetMapping("/category/{id}")
    public APIResult<GetProductsByCategoryWithPageOutput> getProductsByCategoryWithPage(@Validated GetProductsByCategoryWithPageInput input) {
        return productService.getProductsByCategoryWithPage(input);
    }

    @Operation(summary = "查詢產品透過酒品子種類並帶分頁(多筆)")
    @GetMapping("/wineSubType/{id}")
    public APIResult<GetProductsByWineSubTypeWithPageOutput> getProductsByWineSubTypeWithPage(@Validated GetProductsByWineSubTypeInputWithPage input) {
        return productService.getProductsByWineSubTypeWithPage(input);
    }

    @Operation(summary = "查詢產品透過國家並帶分頁(多筆)")
    @GetMapping("/country/{id}")
    public APIResult<GetProductsByCountryWithPageOutput> getProductsByCountryWithPage(@Validated GetProductsByCountryWithPageInput input) {
        return productService.getProductsByCountryWithPage(input);
    }

    @Operation(summary = "查詢產品透過地區並帶分頁(多筆)")
    @GetMapping("/area/{id}")
    public APIResult<GetProductsByAreaWithPageOutput> getProductsByAreaWithPage(@Validated GetProductsByAreaWithPageInput input) {
        return productService.getProductsByAreaWithPage(input);
    }

    @Operation(summary = "查詢產品透過品牌並帶分頁(多筆)")
    @GetMapping("/brand/{id}")
    public APIResult<GetProductsByBrandWithPageOutput> getProductsByBrandWithPage(@Validated GetProductsByBrandWithPageInput input) {
        return productService.getProductsByBrandWithPage(input);
    }

    @Operation(summary = "查詢產品透過葡萄品種並帶分頁(多筆)")
    @GetMapping("/grapeVarieties/{id}")
    public APIResult<GetProductsByGrapeVarietiesWithPageOutput> getProductsByGrapeVarietiesWithPage(@Validated GetProductsByGrapeVarietiesWithPageInput input) {
        return productService.getProductsByGrapeVarietiesWithPage(input);
    }

    @Operation(summary = "查詢產品透過橡木桶並帶分頁(多筆)")
    @GetMapping("/oak/{id}")
    public APIResult<GetProductsByOakWithPageOutput> getProductsByOakWithPage(@Validated GetProductsByOakWithPageInput input) {
        return productService.getProductsByOakWithPage(input);
    }

    @Operation(summary = "查詢產品透過價格區間並帶分頁(多筆)")
    @GetMapping("/price")
    public APIResult<GetProductsByPriceRangeWithPageOutput> getProductsByPriceRangeWithPage(@Validated GetProductsByPriceRangeWithPageInput input) {
        return productService.getProductsByPriceRangeWithPage(input);
    }

    @Operation(summary = "更新產品(單筆")
    @PostMapping("/update")
    public APIResult<UpdateProductOutput> updateProduct(@RequestBody UpdateProductInput input) {
        return productService.updateProduct(input);
    }

    @Operation(summary = "移除產品(單筆")
    @PostMapping("/delete")
    public APIResult<DeleteProductOutput> deleteProduct(@RequestBody DeleteProductInput input) {
        return productService.deleteProduct(input);
    }

    @Operation(summary = "新增產品(單筆")
    @PostMapping("/add")
    public APIResult<AddProductOutput> addProduct(@RequestBody AddProductInput input) {
        return productService.addProduct(input);
    }

}
