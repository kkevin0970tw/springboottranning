package com.hengwei.side2.controller;

import com.hengwei.side2.repository.MemberRepository;
import com.hengwei.side2.repository.PayTypeRepository;
import io.swagger.v3.oas.annotations.Operation;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/testPage")
public class TestPageController {

    @Autowired
    private PayTypeRepository payTypeRepository;

    @Autowired
    private MemberRepository memberRepository;

    @Operation(summary = "購買產品表單頁面(測試用)")
    @GetMapping("/purchasePage")
    public String purchasePage(Model model, Session session) {
        model.addAttribute("payTypes", payTypeRepository.findAll());
        return getPage("purchasePage");
    }

    @Operation(summary = "新增會員(測試用)")
    @GetMapping("/addMemberPage")
    public String addMemberPage(Model model) {
        model.addAttribute("nextSerialize", memberRepository.findMaxId() + 1);
        return getPage("addMemberPage");
    }

    private String getPage(String route) {
        return "testPage/".concat(route);
    }
}
