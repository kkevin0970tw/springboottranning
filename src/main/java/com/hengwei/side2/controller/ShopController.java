package com.hengwei.side2.controller;

import com.hengwei.side2.repository.PayTypeRepository;
import com.hengwei.side2.repository.ProductRepository;
import com.hengwei.side2.service.ShopService;
import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.DoPayInput;
import com.hengwei.side2.vo.input.PurchaseInput;
import com.hengwei.side2.vo.input.TradeInfoInput;
import com.hengwei.side2.vo.output.DoPayOutput;
import com.hengwei.side2.vo.output.PurchaseOutput;
import com.hengwei.side2.vo.output.TradeInfoOutput;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/shop")
@Controller
@ApiResponse(description = "Response Code [ -1:失敗, 0:未知, 1:成功]", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
@Tag(name = "Shop API", description = " 產品購買/退貨 等等交易行為 ")
public class ShopController {

    @Autowired
    private ShopService shopService;

    @Operation(summary = "產品購買(一次處裡多項)")
    @PostMapping("/purchase")
    @ResponseBody
    public APIResult<PurchaseOutput> purchase(@RequestBody @Validated PurchaseInput input) {
        return shopService.purchase(input);
    }

    @Operation(summary = "產品付款 (貨到付款/付款失敗後重新付款)")
    @PostMapping("/doPay")
    @ResponseBody
    public APIResult<DoPayOutput> doPay(@RequestBody @Validated DoPayInput input) {
        return shopService.doPay(input);
    }

    @Operation(summary = "訂單查詢(單筆，不含細節)")
    @PostMapping("/getTradeInfo")
    @ResponseBody
    public APIResult<TradeInfoOutput> getTradeInfo(@RequestBody @Validated TradeInfoInput input) {
        return shopService.tradeInfo(input);
    }
}
