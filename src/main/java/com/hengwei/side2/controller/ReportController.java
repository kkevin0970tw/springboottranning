package com.hengwei.side2.controller;

import com.hengwei.side2.service.ReportService;
import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.GenerateMemberReportInput;
import com.hengwei.side2.vo.input.GenerateProductSalesReportInput;
import com.hengwei.side2.vo.input.GenerateTradeReportInput;
import com.hengwei.side2.vo.output.GenerateMemberReportOutput;
import com.hengwei.side2.vo.output.GenerateProductSalesReportOutput;
import com.hengwei.side2.vo.output.GenerateTradeReportOutput;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @Operation(summary ="會員報表 可查詢單一會員 整體購買紀錄 / 單筆紀錄 / 時間區間紀錄 " )
    @GetMapping("/member/{id}")
    public APIResult<GenerateMemberReportOutput> generateMemberReport(GenerateMemberReportInput input) {
        return reportService.generateMemberReport(input);
    }

    @Operation(summary = "產品銷售報表 (時間 / 可指定產品)  ")
    @GetMapping("/product/{id}")
    public APIResult<GenerateProductSalesReportOutput> generateProductSalesReport(GenerateProductSalesReportInput input) {
        return reportService.generateProductReport(input);
    }

    @Operation(summary = "整體銷售紀錄 不索引會員/產品  屬於綜合紀錄")
    @GetMapping("/trade")
    public APIResult<GenerateTradeReportOutput> generateTradeReport(GenerateTradeReportInput input) {
        return reportService.generateTradeReport(input);
    }

}
