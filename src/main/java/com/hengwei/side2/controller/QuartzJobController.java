package com.hengwei.side2.controller;

import com.hengwei.side2.service.QuartzJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/quartzJob")
public class QuartzJobController {

    @Autowired
    private QuartzJobService quartzJobService;

    @GetMapping("dailyReportBuild")
    public String dailyReportBuild() {
        quartzJobService.dailyReportBuild();
        return "123";
    }
}
