package com.hengwei.side2.cache;

import org.springframework.cache.interceptor.KeyGenerator;

import java.lang.reflect.Method;

public class Cache2KeyGenerator implements KeyGenerator {
    @Override
    public Object generate(Object target, Method method, Object... params) {
        StringBuilder stringBuilder = new StringBuilder();
        String clazzName = "clazzName", id = "id";
        for (Object param : params) {
            if (param instanceof Class clazz) {
                clazzName = clazz.getSimpleName();
            }
            if (param instanceof Integer entityId) {
                id = entityId.toString();
            }
        }
        stringBuilder.append(clazzName);
        stringBuilder.append(id);
        return stringBuilder.toString();
    }
}
