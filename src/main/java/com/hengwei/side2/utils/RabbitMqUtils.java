package com.hengwei.side2.utils;

import com.hengwei.side2.enumeration.RabbitMqEnum;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RabbitMqUtils {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(RabbitMqEnum rabbitMqEnum, String message) {
        sendMessage(rabbitMqEnum.getExchangeName(), rabbitMqEnum.getRouteKey(), message);
    }

    private void sendMessage(String exchange, String routingKey, Object message) {
        rabbitTemplate.convertAndSend(exchange, routingKey, message);
    }
}
