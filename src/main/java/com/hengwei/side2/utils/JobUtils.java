package com.hengwei.side2.utils;

import com.hengwei.side2.enumeration.ExceptionsLevelEnum;
import com.hengwei.side2.utils.LogUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class JobUtils {
    @Autowired
    private LogUtils logUtils;

    private final String LOG_START_FORMAT;
    private final String LOG_FINISH_FORMAT;

    public JobUtils() {
        this.LOG_START_FORMAT = "{} 啟動 ， 時間為 {}";
        this.LOG_FINISH_FORMAT = "{} 結束 ， 時間為 {}";
    }

    public void logStart(Logger log, String simpleName) {
        logUtils.log(log, LOG_START_FORMAT, getArgs(simpleName), ExceptionsLevelEnum.INFO);
    }

    public void logFinish(Logger log, String simpleName) {
        logUtils.log(log, LOG_FINISH_FORMAT, getArgs(simpleName), ExceptionsLevelEnum.INFO);
    }

    private List<Object> getArgs(String simpleName) {
        List<Object> args = new ArrayList<>();
        args.add(simpleName);
        args.add(LocalDateTime.now());
        return args;
    }
}
