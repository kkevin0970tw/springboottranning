package com.hengwei.side2.utils;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Component
public class IOUtils {

    // 上傳檔案測試
    private final String PROFILE_PATH = "C:\\gitproject\\WineShopImg\\";

    public void saveImgFile(String path, MultipartFile imgFile, String account) {
        try {
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String savePath = PROFILE_PATH + path + account;
            imgFile.transferTo(new File(savePath));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
