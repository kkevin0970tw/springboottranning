package com.hengwei.side2.utils;

import com.hengwei.side2.enumeration.ExceptionsLevelEnum;
import com.hengwei.side2.exception.Exceptions;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class LogUtils {

    public void exceptionLog(Logger logger, Exception exception) {
        List<Object> logArgs = new ArrayList<>();
        logArgs.add(exception.getMessage());
        logArgs.add(convertStrace(exception.getStackTrace(), 10));
        String format = "\nException Log---- \n[exception Message]:{} "
                .concat("\n[exception printStackTrace]: \n{}")
                .concat("\n----\n");
        log(logger, format, logArgs, ExceptionsLevelEnum.ERROR);
    }

    public void exceptionsLog(Logger logger, Exceptions exceptions) {
        List<Object> logArgs = new ArrayList<>();
        logArgs.add(exceptions.getMessage());
        if (exceptions.getErrorLog() != null) {
            logArgs.add(exceptions.getErrorLog());
        }
        if (exceptions.isPrintTrace()) {
            logArgs.add(convertStrace(exceptions.getStackTrace(), 10));
        }
        String format = "\nException Log---- \n[exception Message]:{} "
                .concat("\n[errorLog]:{} ")
                .concat("\n[exception printStackTrace]: \n{}")
                .concat("\n----\n");
        log(logger, format, logArgs, exceptions.getExceptionsInstance().getExceptionLevel());
    }

    public void requestLog(Logger logger, HttpServletRequest request) {
        List<Object> logArgs = new ArrayList<>();
        logArgs.add(request.getRequestURI());
        logArgs.add(request.getMethod());

        String format = "Request URL: {}, Method: {} ,";
        switch (request.getMethod()) {
            case "GET" -> {
                if (isParamExistAndCollectGetParameter(request, logArgs)) {
                    format = format.concat(" Request Parameter: {}");
                }
            }
            case "POST" -> {
//                if (isParamExistAndCollectPostBody(request, logArgs)) {
//                    format = format.concat(" Request Body: {}");
//                }
            }
        }
        log(logger, format, logArgs, ExceptionsLevelEnum.INFO);
    }

    public void responseLog(HttpServletResponse response) {
    }

    private StringBuilder convertStrace(StackTraceElement[] stackTrace, int lineLength) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < lineLength; i++) {
            stringBuilder.append(stackTrace[i].toString().concat("\n"));
        }
        return stringBuilder;
    }

    private boolean isParamExistAndCollectGetParameter(HttpServletRequest request, List<Object> logArgs) {
        StringBuilder parameterBuilder = new StringBuilder();
        request.getParameterMap().forEach((k, v) -> parameterBuilder.append(String.format("key: %s, value: %s \t", k, Arrays.toString(v))));
        return parameterBuilder.isEmpty() ? false : logArgs.add(parameterBuilder);
    }

    private boolean isParamExistAndCollectPostBody(HttpServletRequest request, List<Object> logArgs) {
        StringBuilder bodyBuilder = new StringBuilder();
        String tempLine;
        /* 因getReader() 只能調用一次 ， 故調用 ContentCachingRequestWrapper 的 getReader() 才不會報錯*/
        /* 尚待調整 */
        try (BufferedReader reader = new ContentCachingRequestWrapper(request).getReader()) {
            while ((tempLine = reader.readLine()) != null) {
                bodyBuilder.append(tempLine);
            }
        } catch (IOException e) {/*待調 : 確定EXCEPTIONS*/
            throw new RuntimeException(e);
        }
        return bodyBuilder.isEmpty() ? false : logArgs.add(bodyBuilder);
    }

    public void log(Logger logger, String format, List<Object> logArgs, ExceptionsLevelEnum exceptionLevel) {
        switch (exceptionLevel) {
            case INFO -> logger.info(format, logArgs.toArray());
            case WARN -> logger.warn(format, logArgs.toArray());
            case ERROR -> logger.error(format, logArgs.toArray());
        }
    }
}
