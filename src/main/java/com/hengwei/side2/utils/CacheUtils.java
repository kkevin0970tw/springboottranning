package com.hengwei.side2.utils;

import com.hengwei.side2.exception.Exceptions;
import com.hengwei.side2.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

@Component
@Scope("prototype")
public class CacheUtils<T> {

    @Autowired
    private OptionalUtils<T> optionalUtils;

    @Caching(cacheable =
            {
                    @Cacheable(value = "cache1", keyGenerator = "cache1KeyGenerator", cacheManager = "caffeineCacheManager"),
                    @Cacheable(value = "cache1", keyGenerator = "cache1KeyGenerator", cacheManager = "redisCacheManager")
            }
    )
    public T getEntityFromCache(JpaRepository<T, Integer> repository, Integer id, Class<T> clazz  /*用於 keyGenerator key組合*/) {
        return optionalUtils.findOneOrThrow(
                id,
                repository.findById(id),
                Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
        );
    }


    @Caching(cacheable =
            {
                    @Cacheable(value = "cache2", keyGenerator = "cache2KeyGenerator", cacheManager = "caffeineCacheManager"),
                    @Cacheable(value = "cache2", keyGenerator = "cache2KeyGenerator", cacheManager = "redisCacheManager")
            }
    )
    public T getDataFromCache2(JpaRepository<T, Integer> repository, Integer id, Class<T> clazz  /*用於 keyGenerator key組合*/) {
        return optionalUtils.findOneOrThrow(
                id,
                repository.findById(id),
                Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
        );
    }

    @Caching(cacheable =
            {
                    @Cacheable(value = "cache2", keyGenerator = "cache2KeyGenerator", cacheManager = "caffeineCacheManager"),
                    @Cacheable(value = "cache2", keyGenerator = "cache2KeyGenerator", cacheManager = "redisCacheManager")
            }
    )
    public String getProductNameFromCache2(ProductRepository repository, Integer id, Class<String> stringClass) {
        return optionalUtils.findOneOrThrow(
                id,
                repository.findNameById(id),
                Exceptions.DataBaseException.DATA_BASE_DATA_NOT_EXIST
        );
    }
}