package com.hengwei.side2.utils;

import com.hengwei.side2.exception.ExceptionsBuilder;
import com.hengwei.side2.exception.ExceptionsInterface;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Scope("prototype")
public class OptionalUtils<T> {

    public T findOneOrThrow(Optional<T> optional, ExceptionsInterface exceptions) {
        return findOneOrThrow(null, optional, exceptions);
    }

    public T findOneOrThrow(Object input, Optional<T> optional, ExceptionsInterface exceptions) {
        return optional.orElseThrow(
                () -> ExceptionsBuilder.builder(exceptions)
                        .setErrorLog(input == null ? "This API Without Input" : input.toString())
                        .build()
        );
    }

    public List<T> findMoreOrThrow(List<T> list, ExceptionsInterface exceptions) {
        return findMoreOrThrow(null, Optional.of(list), exceptions);
    }

    public List<T> findMoreOrThrow(Object input, List<T> list, ExceptionsInterface exceptions) {
        return findMoreOrThrow(input, Optional.of(list), exceptions);
    }

    private List<T> findMoreOrThrow(Object input, Optional<List<T>> optional, ExceptionsInterface exceptions) {
        if (optional.isPresent()) {
            if (optional.get().isEmpty()) {
                throw ExceptionsBuilder.builder(exceptions)
                        .setErrorLog(input == null ? "This API Without Input" : input.toString())
                        .build();
            }
        } else {
            throw ExceptionsBuilder.builder(exceptions)
                    .setErrorLog(input == null ? "This API Without Input" : input.toString())
                    .build();
        }
        return optional.get();
    }


    public void existThenThrow(Object input, Optional<T> optional, ExceptionsInterface exceptions) {
        if (optional.isPresent()) {
            throw ExceptionsBuilder.builder(exceptions)
                    .setErrorLog(input.toString())
                    .build();
        }
    }

    public void notExistThenThrow(Object input, Optional<T> optional, ExceptionsInterface exceptions) {
        if (optional.isEmpty()) {
            throw ExceptionsBuilder.builder(exceptions)
                    .setErrorLog(input.toString())
                    .build();
        }
    }

    public Page<T> findMoreWithPageOrThrow(Object input, Page<T> pageResult, ExceptionsInterface exceptions) {
        if (pageResult.getContent().isEmpty()) {
            throw ExceptionsBuilder.builder(exceptions)
                    .setErrorLog(input.toString())
                    .build();
        }
        return pageResult;
    }

    public String findOneOrThrow(Integer input, Optional<String> optional, ExceptionsInterface exceptions) {
        if (optional.isEmpty()) {
            throw ExceptionsBuilder.builder(exceptions)
                    .setErrorLog(input.toString())
                    .build();
        }
        return optional.get();
    }
}
