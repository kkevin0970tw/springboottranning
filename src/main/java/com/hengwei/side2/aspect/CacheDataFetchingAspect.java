package com.hengwei.side2.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CacheDataFetchingAspect {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Before("execution(* com.hengwei.side2.utils.CacheUtils.*(..))")
    public void beforeMethodExecution(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("Before : 因資料尚未有 cache 存在 故從資料庫獲取資料: {}", methodName);
    }

    @AfterReturning(pointcut = "execution(* com.hengwei.side2.utils.CacheUtils.*(..))", returning = "result")
    public void afterMethodExecution(JoinPoint joinPoint, Object result) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("After 資料庫獲取資料完畢: {},{}", methodName, result);
    }
}
