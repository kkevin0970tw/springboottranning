package com.hengwei.side2.listener;

import com.hengwei.side2.bootstrap.TestLoader;
import org.springframework.boot.ConfigurableBootstrapContext;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.time.Duration;

public class CustomAppListener implements SpringApplicationRunListener {

    @Override
    public void starting(ConfigurableBootstrapContext bootstrapContext) {
        System.out.println("starting 階段 (應用程序正在啟動) ");
        TestLoader testLoader = bootstrapContext.get(TestLoader.class);
        System.out.println(testLoader.getLuckyNum());
    }

    @Override
    public void environmentPrepared(ConfigurableBootstrapContext bootstrapContext, ConfigurableEnvironment environment) {
        System.out.println("environmentPrepared 階段 (環境準備已經完成) ");
        TestLoader testLoader = bootstrapContext.get(TestLoader.class);
        System.out.println(testLoader.getLuckyNum());


//        environment.getSystemEnvironment().forEach((k, v) -> System.out.printf("key : %s  , value : %s \n", k, v));
//        System.out.println("bootstrapContext = " + bootstrapContext + ", environment = " + environment);
//        environment.getSystemProperties().forEach((k, v) -> System.out.printf("key : %s  , value : %s \n", k, v));
    }

    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {
        System.out.println("contextPrepared 階段 (ioc容器準備完成) ");
    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
        System.out.println("contextLoaded 階段 (ioc容器載入完成) ");
    }

    @Override
    public void started(ConfigurableApplicationContext context, Duration timeTaken) {
        System.out.printf("started 階段 (應用程序已經啟動) sec:%d\n", timeTaken.getSeconds());
    }

    @Override
    public void ready(ConfigurableApplicationContext context, Duration timeTaken) {
        System.out.printf("ready 階段 (應用程序正在運行) sec:%d\n", timeTaken.getSeconds());
    }

    @Override
    public void failed(ConfigurableApplicationContext context, Throwable exception) {
        System.out.println("failed 階段 (應用程序啟動失敗");
    }
}
