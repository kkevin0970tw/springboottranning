package com.hengwei.side2.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.beans.factory.annotation.Value;

@OpenAPIDefinition
        (
                info = @io.swagger.v3.oas.annotations.info.Info
                        (
                                title = "Swagger API UI",
                                description = "API 測試介面",
                                version = "V1",
                                termsOfService = "",
                                contact = @io.swagger.v3.oas.annotations.info.Contact
                                        (
                                                name = "HENG WEI",
                                                email = "asfhzhf324hsdfhas@gmail.com",
                                                url = ""
                                        ),
                                license = @io.swagger.v3.oas.annotations.info.License
                                        (
                                                name = "Apache License, Version 2.0",
                                                url = ""
                                        )

                        )
        )
public class SwaggerConfig {
    @Value("${swagger.enabled}")
    private boolean enable;
}
