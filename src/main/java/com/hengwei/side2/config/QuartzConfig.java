package com.hengwei.side2.config;


import com.hengwei.side2.job.AutoBuyJob;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfig {
    private JobDataMap getDataMap() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("expectTime", 5);
        jobDataMap.put("timeLogFlag", false);
        return jobDataMap;
    }

    @Bean(name = "autoBuyJob")
    public JobDetail autoBuyJob() {
        return JobBuilder.newJob(AutoBuyJob.class)
                .withIdentity("autoBuyJob", "groupA1")
                .withDescription("定時假裝有客人購買")
                .setJobData(getDataMap())
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger autoBuyTrigger(@Qualifier("autoBuyJob") JobDetail jobDetail) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .startNow()
                .withIdentity("autoBuyJobTrigger", "groupA1")
                .withDescription("五秒觸發一次/永久")
                .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(10))
                .build();
    }


    @Bean(name = "dailyReportBuildJob")
    public JobDetail dailyReportBuildJob() {
        return JobBuilder.newJob(AutoBuyJob.class)
                .withIdentity("dailyReportBuildJob", "groupA2")
                .withDescription("定時假裝有客人購買")
                .setJobData(getDataMap())
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger dailyReportBuildTrigger(@Qualifier("dailyReportBuildJob") JobDetail jobDetail) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .startNow()
                .withIdentity("dailyReportBuildTrigger", "groupA2")
                .withDescription("每天凌晨1:30點觸發/永久")
                .withSchedule(CronScheduleBuilder.cronSchedule("0 30 1 * * ?"))
                .build();
    }
}
