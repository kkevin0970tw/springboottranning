package com.hengwei.side2.config;

import com.hengwei.side2.enumeration.RoleEnum;
import com.hengwei.side2.security.service.UserDetailServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return Argon2PasswordEncoder.defaultsForSpringSecurity_v5_8();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailServiceImpl();
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService());
        provider.setPasswordEncoder(passwordEncoder());
        return provider;
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new com.hengwei.side2.security.handler.AuthenticationSuccessHandler();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
                .csrf(AbstractHttpConfigurer::disable)
                .cors(AbstractHttpConfigurer::disable)
                .authenticationProvider(authenticationProvider())
                .authorizeHttpRequests(
                        authorizationManagerRequestMatcherRegistry ->
                                authorizationManagerRequestMatcherRegistry
                                        .requestMatchers("/css/**").permitAll()
                                        .requestMatchers("/security/form-login/**").permitAll()
                                        .requestMatchers("/security/user1/security-auth-test").hasAnyAuthority(RoleEnum.getSecurityAuthority(1))
                                        .requestMatchers("/security/user2/security-auth-test").hasAnyAuthority(RoleEnum.getSecurityAuthority(2))
                                        .requestMatchers("/security/user3/security-auth-test").hasAnyAuthority(RoleEnum.getSecurityAuthority(3))
                                        .requestMatchers("/security/user4/security-auth-test").hasAnyAuthority(RoleEnum.getSecurityAuthority(4))
                                        .requestMatchers("/security/user5/security-auth-test").hasAnyAuthority(RoleEnum.getSecurityAuthority(5))
                                        .requestMatchers("/security/admin/security-auth-test").hasAnyAuthority(RoleEnum.getSecurityAuthority(6))
                                        .requestMatchers("/security/root/security-auth-test").hasAnyAuthority(RoleEnum.getSecurityAuthority(7))
                                        .requestMatchers("/swagger**").hasAuthority(RoleEnum.ROOT.name())
                                        .requestMatchers("/testPage**").hasAuthority(RoleEnum.ROOT.name())
                                        .anyRequest().hasAuthority(RoleEnum.ROOT.name())
                )
                .formLogin(
                        httpSecurityFormLoginConfigurer ->
                                httpSecurityFormLoginConfigurer
                                        .loginPage("/security/form-login/login")                             /*登入頁面的API*/
                                        .loginProcessingUrl("/security/form-login/loginProcessingUrl")       /*驗證登入的網址(此設定後 表單Action指向這網址即可，邏輯由Spring自行執行)*/
                                        .failureForwardUrl("/security/form-login/authentication-fail")       /*登入失敗後前往的地方*/
                                        .successHandler(authenticationSuccessHandler())                      /*成功登入後前往的地方*/
                                        .permitAll()
                )
                .exceptionHandling(httpSecurityExceptionHandlingConfigurer ->
                        httpSecurityExceptionHandlingConfigurer
                                .accessDeniedPage("/security/page403")   /* 登入後有權限方面問題而不能訪問的頁面 將會轉移至此頁面*/
                )
                .build();
    }
}
