package com.hengwei.side2.config;

import com.hengwei.side2.enumeration.RabbitMqEnum;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
public class RabbitMqConfig {
    // 請透過 RabbitMqEnum 設定即可
    @Bean
    public Declarables declarables(Queue[] queues, Exchange[] exchanges) {
        Declarables declarables = new Declarables();
        Collection<Declarable> declarableCollection = declarables.getDeclarables();

        Map<String, Queue> queueMap = Stream.of(queues).collect(Collectors.toMap(Queue::getName, queue -> queue));
        Map<String, Exchange> exchangeMap = Stream.of(exchanges).collect(Collectors.toMap(Exchange::getName, exchange -> exchange));

        declarableCollection.addAll(List.of(queues));
        declarableCollection.addAll(List.of(exchanges));
        for (RabbitMqEnum rabbitMq : RabbitMqEnum.values()) {
            for (String queueName : rabbitMq.getQueueNames()) {
                declarables.getDeclarables().add(
                        BindingBuilder.
                                bind(queueMap.get(queueName)).
                                to(exchangeMap.get(rabbitMq.getExchangeName())).
                                with(rabbitMq.getRouteKey()).noargs());
            }
        }
        return declarables;
    }

    @Bean
    public Queue[] getQueues() {
        return new Queue[]{
                new Queue("managerQueue", true),
                new Queue("userQueue", true)
        };
    }

    @Bean
    public Exchange[] getExchanges() {
        RabbitMqEnum[] rabbits = RabbitMqEnum.values();
        Exchange[] exchanges = new Exchange[rabbits.length];
        for (int i = 0; i < rabbits.length; i++) {
            exchanges[i] = selectExchange(rabbits[i]);
        }
        return exchanges;
    }

    private Exchange selectExchange(RabbitMqEnum rabbitMqEnum) {
        String exchangeName = rabbitMqEnum.getExchangeName();
        return switch (rabbitMqEnum.getExchangeType().getSimpleName()) {
            case "FanoutExchange" -> new FanoutExchange(exchangeName, true, false);
            case "TopicExchange" -> new TopicExchange(exchangeName, true, false);
            case "HeadersExchange" -> new HeadersExchange(exchangeName, true, false);
            case "DirectExchange" -> new DirectExchange(exchangeName, true, false);
            default -> new DirectExchange(rabbitMqEnum.getExchangeName(), true, false);
        };
    }
}
