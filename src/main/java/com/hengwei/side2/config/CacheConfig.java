package com.hengwei.side2.config;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.hengwei.side2.cache.Cache1KeyGenerator;
import com.hengwei.side2.cache.Cache2KeyGenerator;
import com.hengwei.side2.enumeration.ExceptionsLevelEnum;
import com.hengwei.side2.interceptor.SecondaryCacheInterceptor;
import com.hengwei.side2.utils.LogUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.AnnotationCacheOperationSource;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.interceptor.CacheInterceptor;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.support.CompositeCacheManager;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Configuration
@EnableCaching
public class CacheConfig {
    @Autowired
    private LogUtils logUtils;

    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    @Bean
    public CacheManager cacheManager() {
        CompositeCacheManager cacheManager = new CompositeCacheManager(caffeineCacheManager(), redisCacheManager());
        cacheManager.setFallbackToNoOpCache(true);
        return cacheManager;
    }

    @Bean
    public RedisCacheManager redisCacheManager() {
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(new GenericJackson2JsonRedisSerializer()))
                .entryTtl(Duration.ofSeconds(600));
        return RedisCacheManager.builder(redisConnectionFactory)
                .cacheDefaults(redisCacheConfiguration)
                .build();
    }

    @Bean
    @Primary
    public SimpleCacheManager caffeineCacheManager() {
        SimpleCacheManager caffeineCacheManager = new SimpleCacheManager();
        caffeineCacheManager.setCaches(caches());
        return caffeineCacheManager;
    }

    private CaffeineCache cacheBuilder(String cacheName, int day) {
        return new CaffeineCache(
                cacheName, Caffeine.newBuilder()
                .initialCapacity(150)
                .maximumSize(1500)
                .expireAfterWrite(Duration.ofDays(day))
                .removalListener((key, value, cause) -> {
                    String format = ">>> Cache Clean [{}]({}), reason is [{}]";
                    List<Object> logArgs = new ArrayList<>();
                    logArgs.add(key);
                    logArgs.add(value);
                    logArgs.add(cause);
                    logUtils.log(LoggerFactory.getLogger(this.getClass().getName()), format, logArgs, ExceptionsLevelEnum.INFO);
                })
                .recordStats()
                .build()
        );
    }

    private Collection<Cache> caches() {
        List<Cache> cacheList = new ArrayList<>();
        cacheList.add(cacheBuilder("cache1", 4));
        cacheList.add(cacheBuilder("cache2", 365));
        cacheList.add(cacheBuilder("cache3", 24));
        return cacheList;
    }

    @Bean
    public KeyGenerator cache1KeyGenerator() {
        return new Cache1KeyGenerator();
    }

    @Bean
    public KeyGenerator cache2KeyGenerator() {
        return new Cache2KeyGenerator();
    }

    @Bean
    @Primary
    public CacheInterceptor secondaryCacheInterceptor(CacheManager cacheManager) {
        CacheInterceptor interceptor = new SecondaryCacheInterceptor(cacheManager);
        interceptor.setCacheOperationSource(new AnnotationCacheOperationSource());
        return interceptor;
    }
}
