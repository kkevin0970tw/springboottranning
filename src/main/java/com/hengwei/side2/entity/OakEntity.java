package com.hengwei.side2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tbl_oak", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OakEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fld_id")
    private Integer id;

    @Column(name = "fld_name_en")
    private String nameEN;

    @Column(name = "fld_name_tw")
    private String nameTW;
}
