package com.hengwei.side2.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportDailyProductSalesPK {
    @Column(name = "fld_date")
    private LocalDate date;

    @Column(name = "fld_product_id")
    private BigDecimal productId;

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportDailyProductSalesPK that = (ReportDailyProductSalesPK) o;
        return Objects.equals(date, that.date) && Objects.equals(productId, that.productId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, productId);
    }
}
