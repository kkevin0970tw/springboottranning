package com.hengwei.side2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tbl_manager", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ManagerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "fld_account")
    private String account;

    @Column(name = "fld_password")
    private String password;

    @Column(name = "fld_name")
    private String name;

    @Column(name = "fld_role_id")
    private int roleId;
}
