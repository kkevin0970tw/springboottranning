package com.hengwei.side2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Entity
@Table(name = "tbl_report_daily_pay_type_count", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportDailyPayTypeCountEntity {
    @EmbeddedId
    private ReportDailyPayTypeCountPK pk;

    @Column(name = "fld_count")
    private BigDecimal count;
}
