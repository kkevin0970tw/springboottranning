package com.hengwei.side2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

@Entity
@Table(name = "tbl_brand", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BrandEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fld_id")
    private int id;

    @Column(name = "fld_name_en")
    private String nameEN;

    @Column(name = "fld_name_tw")
    private String nameTW;
}
