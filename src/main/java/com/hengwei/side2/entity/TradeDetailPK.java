package com.hengwei.side2.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TradeDetailPK implements Serializable {

    @Column(name = "fld_trade_id")
    private Integer tradeId;

    @Column(name = "fld_product_id")
    private Integer productId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TradeDetailPK that = (TradeDetailPK) o;
        return Objects.equals(tradeId, that.tradeId) && Objects.equals(productId, that.productId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tradeId, productId);
    }
}
