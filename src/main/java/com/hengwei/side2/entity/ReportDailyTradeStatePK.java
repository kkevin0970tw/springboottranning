package com.hengwei.side2.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportDailyTradeStatePK {
    @Column(name = "fld_date")
    private LocalDate date;

    @Column(name = "fld_trade_state_id")
    private BigDecimal tradeStateId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportDailyTradeStatePK that = (ReportDailyTradeStatePK) o;
        return Objects.equals(date, that.date) && Objects.equals(tradeStateId, that.tradeStateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, tradeStateId);
    }
}
