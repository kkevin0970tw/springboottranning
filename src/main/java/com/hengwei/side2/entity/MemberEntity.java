package com.hengwei.side2.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "tbl_member", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MemberEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fld_id")
    private Integer id;

    @Column(name = "fld_name")
    private String name;

    @Column(name = "fld_email")
    private String email;

    @Column(name = "fld_phone_number")
    private String phoneNumber;

    @Column(name = "fld_date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "fld_registration_date")
    private LocalDateTime registrationDate;

    @Column(name = "fld_last_login")
    private LocalDateTime lastLogin;

    @Column(name = "fld_profile_picture")
    private String profilePicture;

    @Column(name = "fld_account")
    private String account;

    @Column(name = "fld_password")
    private String password;

    @Column(name = "fld_role_id")
    private Integer roleId;

}
