package com.hengwei.side2.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Objects;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportDailyPayTypeCountPK {
    @Column(name = "fld_date")
    private LocalDate date;

    @Column(name = "fld_pay_type_id")
    private Byte payTypeId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportDailyPayTypeCountPK that = (ReportDailyPayTypeCountPK) o;
        return Objects.equals(date, that.date) && Objects.equals(payTypeId, that.payTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, payTypeId);
    }
}
