package com.hengwei.side2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tbl_trade_state", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TradeStateEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fld_id")
    private int id;

    @Column(name = "fld_state_tw")
    private String stateTW;

    @Column(name = "fld_state_en")
    private String stateEN;

}
