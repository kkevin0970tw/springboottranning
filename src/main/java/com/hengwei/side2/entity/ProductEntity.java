package com.hengwei.side2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Persistable;

import java.time.LocalDateTime;

@Entity
@Table(name = "tbl_product")
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductEntity implements Persistable<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fld_id")
    private Integer id;

    @Getter
    @Column(name = "fld_name")
    private String name;

    @Getter
    @Column(name = "fld_category_id")
    private Integer categoryId;

    @Getter
    @Column(name = "fld_wine_sub_type_id")
    private Integer wineSubTypeId;

    @Getter
    @Column(name = "fld_country_id")
    private Integer countryId;

    @Getter
    @Column(name = "fld_area_id")
    private Integer areaId;

    @Getter
    @Column(name = "fld_brand_id")
    private Integer brandId;

    @Getter
    @Column(name = "fld_grape_varieties_id")
    private Integer grapeVarietiesId;

    @Getter
    @Column(name = "fld_oak_id")
    private Integer oakId;

    @Getter
    @Column(name = "fld_img_path")
    private String imgPath;

    @Getter
    @Column(name = "fld_price")
    private Integer price;

    @Getter
    @Column(name = "fld_capacity")
    private Short capacity;

    @Getter
    @Column(name = "fld_description")
    private String description;

    @Getter
    @Column(name = "fld_show")
    private Boolean show;

    @Getter
    @Column(name = "fld_update_datetime")
    private LocalDateTime updateDateTime;

    @Getter
    @Column(name = "fld_discount")
    private Integer discount;

    @Transient
    private boolean isNew = false;


    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public boolean isNew() {
        return this.isNew;
    }

    public void markNew() {
        this.isNew = true;
    }
}
