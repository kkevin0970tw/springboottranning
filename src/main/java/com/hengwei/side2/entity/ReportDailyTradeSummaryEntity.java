package com.hengwei.side2.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "tbl_report_daily_trade_summary", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportDailyTradeSummaryEntity {
    @Id
    @Column(name = "fld_date")
    private LocalDate date;

    @Column(name = "fld_total_count")
    private BigDecimal totalCount;

    @Column(name = "fld_total_amount")
    private BigDecimal totalAmount;

    @Column(name = "fld_total_discount")
    private BigDecimal totalDiscount;
}
