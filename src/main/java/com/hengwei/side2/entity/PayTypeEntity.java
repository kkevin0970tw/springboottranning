package com.hengwei.side2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tbl_pay_type", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PayTypeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fld_id")
    private Integer id;

    @Column(name = "fld_key_name")
    private String keyName;

    @Column(name = "fld_name_en")
    private String nameEN;

    @Column(name = "fld_name_tw")
    private String nameTW;

    @Column(name = "fld_enable")
    private Boolean enable;
}
