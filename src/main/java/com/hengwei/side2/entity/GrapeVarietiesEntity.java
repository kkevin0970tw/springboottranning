package com.hengwei.side2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tbl_grape_varieties", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GrapeVarietiesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fld_id")
    private int id;

    @Column(name = "fld_name_en")
    private String nameEN;

    @Column(name = "fld_name_tw")
    private String nameTW;
}
