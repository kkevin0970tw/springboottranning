package com.hengwei.side2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tbl_wine_sub_type", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WineSubTypeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fld_id")
    private int id;

    @Column(name = "fld_name_en")
    private String nameEN;

    @Column(name = "fld_name_tw")
    private String nameTW;
}
