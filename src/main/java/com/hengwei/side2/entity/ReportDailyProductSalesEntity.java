package com.hengwei.side2.entity;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Entity
@Table(name = "tbl_report_daily_product_sales", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportDailyProductSalesEntity {
    @EmbeddedId
    private ReportDailyProductSalesPK pk;

    @Column(name = "fld_count")
    private BigDecimal count;

    @Column(name = "fld_total_amount")
    private BigDecimal totalAmount;
}
