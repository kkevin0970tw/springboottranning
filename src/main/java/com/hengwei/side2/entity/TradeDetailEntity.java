package com.hengwei.side2.entity;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tbl_trade_detail", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TradeDetailEntity {

    @EmbeddedId
    private TradeDetailPK pk;

    @Column(name = "fld_price")
    private Integer price;

    @Column(name = "fld_discount")
    private Integer discount;

    @Column(name = "fld_quantity")
    private Integer quantity;

}
