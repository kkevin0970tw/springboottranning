package com.hengwei.side2.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Table(name = "tbl_trade", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TradeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fld_id")
    private Integer id;

    @Column(name = "fld_member_id")
    private Integer memberId;

    @Column(name = "fld_trade_datetime")
    private LocalDateTime tradeDateTime;

    @Column(name = "fld_trade_amount")
    private Integer tradeAmount;

    @Column(name = "fld_trade_amount_with_discount")
    private Integer tradeAmountWithDiscount;

    @Column(name = "fld_trade_state_id")
    private Integer tradeStateId;

    @Column(name = "fld_pay_type_id")
    private Integer payTypeId;

    @Column(name = "fld_pay_datetime")
    private LocalDateTime payDateTime;
}
