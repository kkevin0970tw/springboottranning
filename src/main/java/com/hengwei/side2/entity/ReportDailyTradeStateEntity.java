package com.hengwei.side2.entity;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Entity
@Table(name = "tbl_report_daily_trade_state", schema = "wineorder")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportDailyTradeStateEntity {

    @EmbeddedId
    private ReportDailyTradeStatePK pk;

    @Column(name = "fld_count")
    private BigDecimal count;

    @Column(name = "fld_total_amount")
    private BigDecimal totalAmount;

    @Column(name = "fld_total_amount_with_discount")
    private BigDecimal totalAmountWithDiscount;
}
