package com.hengwei.side2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class Side2Application {
    @Autowired
    private Environment environment;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Side2Application.class, args);
        Side2Application app = context.getBean(Side2Application.class);
        app.printActiveProfile();
    }

    public void printActiveProfile() {
        String[] activeProfiles = environment.getActiveProfiles();
        System.out.println("環境 [ " + String.join(",", activeProfiles) + "] ");
    }
}