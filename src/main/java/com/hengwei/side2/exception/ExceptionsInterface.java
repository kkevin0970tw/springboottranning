package com.hengwei.side2.exception;

import com.hengwei.side2.enumeration.ExceptionsLevelEnum;

public interface ExceptionsInterface {
    int getCode();

    String getMessage();

    ExceptionsLevelEnum getExceptionLevel();

    void setExceptionLevel(ExceptionsLevelEnum exceptionLevelEnum);
}
