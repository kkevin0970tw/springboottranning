package com.hengwei.side2.exception;

import com.hengwei.side2.enumeration.ExceptionsLevelEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Exceptions extends RuntimeException {

    private String customMessage;
    private String errorLog;
    private Throwable cause;
    private boolean isPrintTrace;
    private ExceptionsInterface exceptionsInstance;

    Exceptions(ExceptionsInterface exceptionsInstance) {
        super(exceptionsInstance.getMessage());
        this.exceptionsInstance = exceptionsInstance;
    }

    Exceptions(ExceptionsInterface exceptionsInstance, Throwable cause) {
        super(exceptionsInstance.getMessage());
        this.exceptionsInstance = exceptionsInstance;
        this.cause = cause;
    }

    public int getCode() {
        return exceptionsInstance.getCode();
    }

    public enum ParamException implements ExceptionsInterface {
        PARAM_EXCEPTION(1000, "參數錯誤", ExceptionsLevelEnum.WARN);
        private final int code;
        private final String message;
        private ExceptionsLevelEnum exceptionLevelEnum;

        ParamException(int code, String message, ExceptionsLevelEnum exceptionsLevelEnum) {
            this.code = code;
            this.message = message;
            this.exceptionLevelEnum = exceptionsLevelEnum;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ExceptionsLevelEnum getExceptionLevel() {
            return exceptionLevelEnum;
        }

        @Override
        public void setExceptionLevel(ExceptionsLevelEnum exceptionLevelEnum) {
            this.exceptionLevelEnum = exceptionLevelEnum;
        }
    }

    public enum DataBaseException implements ExceptionsInterface {
        DATA_BASE_EXCEPTION(2000, "資料庫操作有誤", ExceptionsLevelEnum.WARN),
        DATA_BASE_DATA_ALREADY_EXIST(2001, "資料已經存在", ExceptionsLevelEnum.WARN),
        DATA_BASE_DATA_NOT_EXIST(2002, "資料不存在", ExceptionsLevelEnum.WARN),
        DATA_BASE_CONSTRAINT_FOREIGN_KEY(2003, "外來鍵約束", ExceptionsLevelEnum.WARN),
        DATA_BASE_CONSISTENT_ERROR(2004, "資料不一致(嚴重)", ExceptionsLevelEnum.ERROR);
        private final int code;
        private final String message;
        private ExceptionsLevelEnum exceptionLevelEnum;

        DataBaseException(int code, String message, ExceptionsLevelEnum exceptionsLevelEnum) {
            this.code = code;
            this.message = message;
            this.exceptionLevelEnum = exceptionsLevelEnum;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ExceptionsLevelEnum getExceptionLevel() {
            return exceptionLevelEnum;
        }

        @Override
        public void setExceptionLevel(ExceptionsLevelEnum exceptionLevelEnum) {
            this.exceptionLevelEnum = exceptionLevelEnum;
        }
    }

    public enum ResultException implements ExceptionsInterface {
        RESULT_EXCEPTION(3000, "結果出現未知問題", ExceptionsLevelEnum.WARN),
        RESULT_NOT_FOUND(3001, "查無結果", ExceptionsLevelEnum.WARN);
        private final int code;
        private final String message;
        private ExceptionsLevelEnum exceptionLevelEnum;

        ResultException(int code, String message, ExceptionsLevelEnum exceptionsLevelEnum) {
            this.code = code;
            this.message = message;
            this.exceptionLevelEnum = exceptionsLevelEnum;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ExceptionsLevelEnum getExceptionLevel() {
            return exceptionLevelEnum;
        }

        @Override
        public void setExceptionLevel(ExceptionsLevelEnum exceptionLevelEnum) {
            this.exceptionLevelEnum = exceptionLevelEnum;
        }
    }

    public enum LogException implements ExceptionsInterface {
        LOG_EXCEPTION(9000, "紀錄LOG有問題", ExceptionsLevelEnum.WARN),
        LOG_POST_BODY_EXCEPTION(9001, "擷取POST BODY 出問題", ExceptionsLevelEnum.WARN);
        private final int code;
        private final String message;
        private ExceptionsLevelEnum exceptionLevelEnum;

        LogException(int code, String message, ExceptionsLevelEnum exceptionsLevelEnum) {
            this.code = code;
            this.message = message;
            this.exceptionLevelEnum = exceptionsLevelEnum;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ExceptionsLevelEnum getExceptionLevel() {
            return exceptionLevelEnum;
        }

        @Override
        public void setExceptionLevel(ExceptionsLevelEnum exceptionLevelEnum) {
            this.exceptionLevelEnum = exceptionLevelEnum;
        }
    }

    public enum TestException implements ExceptionsInterface {
        TEST_EXCEPTION(8000, "測試程式發生預期外事件", ExceptionsLevelEnum.WARN),
        TEST_CONVERT_PARAM_FAIL(8001, "測試程式轉換GET param 錯誤", ExceptionsLevelEnum.WARN);
        private final int code;
        private final String message;
        private ExceptionsLevelEnum exceptionLevelEnum;

        TestException(int code, String message, ExceptionsLevelEnum exceptionsLevelEnum) {
            this.code = code;
            this.message = message;
            this.exceptionLevelEnum = exceptionsLevelEnum;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ExceptionsLevelEnum getExceptionLevel() {
            return exceptionLevelEnum;
        }

        @Override
        public void setExceptionLevel(ExceptionsLevelEnum exceptionLevelEnum) {
            this.exceptionLevelEnum = exceptionLevelEnum;
        }
    }

    public enum AccountException implements ExceptionsInterface {
        ACCOUNT_EXCEPTION(7000, "測試程式發生預期外事件", ExceptionsLevelEnum.WARN),
        ACCOUNT_NOT_EXIST(7001, "帳號不存在", ExceptionsLevelEnum.WARN),
        PASSWORD_NOT_EQUAL(7002, "密碼比對錯誤", ExceptionsLevelEnum.WARN);
        private final int code;
        private final String message;
        private ExceptionsLevelEnum exceptionLevelEnum;

        AccountException(int code, String message, ExceptionsLevelEnum exceptionsLevelEnum) {
            this.code = code;
            this.message = message;
            this.exceptionLevelEnum = exceptionsLevelEnum;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ExceptionsLevelEnum getExceptionLevel() {
            return exceptionLevelEnum;
        }

        @Override
        public void setExceptionLevel(ExceptionsLevelEnum exceptionLevelEnum) {
            this.exceptionLevelEnum = exceptionLevelEnum;
        }
    }

    public enum AuthorizationException implements ExceptionsInterface {
        AUTHORIZATION_EXCEPTION(7000, "驗證失敗", ExceptionsLevelEnum.WARN);
        private final int code;
        private final String message;
        private ExceptionsLevelEnum exceptionLevelEnum;

        AuthorizationException(int code, String message, ExceptionsLevelEnum exceptionsLevelEnum) {
            this.code = code;
            this.message = message;
            this.exceptionLevelEnum = exceptionsLevelEnum;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ExceptionsLevelEnum getExceptionLevel() {
            return exceptionLevelEnum;
        }

        @Override
        public void setExceptionLevel(ExceptionsLevelEnum exceptionLevelEnum) {
            this.exceptionLevelEnum = exceptionLevelEnum;
        }
    }
}
