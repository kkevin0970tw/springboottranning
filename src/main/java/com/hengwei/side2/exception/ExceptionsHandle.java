package com.hengwei.side2.exception;

import com.hengwei.side2.utils.LogUtils;
import com.hengwei.side2.vo.common.APIExceptionResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

@ControllerAdvice
@ResponseBody
public class ExceptionsHandle {

    private final Logger logger = LoggerFactory.getLogger((this.getClass().getName()));

    @Autowired
    private LogUtils logUtils;

    @ExceptionHandler(value = Exception.class)
    public APIExceptionResult exceptionsHandle(Exception exception) {
        logUtils.exceptionLog(logger, exception);
        return new APIExceptionResult(exception);
    }

    @ExceptionHandler(value = Exceptions.class)
    public APIExceptionResult exceptionsHandle(Exceptions exceptions) {
        logUtils.exceptionsLog(logger, exceptions);
        return new APIExceptionResult(exceptions);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public APIExceptionResult requestParamExceptionsHandle(Exception exception) {
        logUtils.exceptionLog(logger, exception);
        return new APIExceptionResult(
                ExceptionsBuilder.builder(Exceptions.ParamException.PARAM_EXCEPTION)
                        .setCustomMessage(splitExceptionMessage(exception.getMessage()).toString())
                        .setErrorLog("輸入參數規則有誤，請看客製訊息")
                        .build()
        );
    }

    private StringBuilder splitExceptionMessage(String message) {
        StringBuilder stringBuilder = new StringBuilder();
        Arrays.stream(message.split(";")).forEach(str ->
                stringBuilder.append(str.concat("\n")));
        return stringBuilder;
    }


}