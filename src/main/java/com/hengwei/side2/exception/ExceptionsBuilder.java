package com.hengwei.side2.exception;

public class ExceptionsBuilder {
    private String customMessage /*自訂訊息，並丟給API請求方的訊息*/ = "無客製訊息,請參照 ExceptionsMessage或通知管理員";
    private String errorLog      /*將記錄到Log的訊息，API請求方看不到，給後台紀錄使用*/;
    private Throwable cause      /**/;
    private boolean isPrintTrace /*是否列印出資訊，預設true*/ = true;
    private final ExceptionsInterface exceptionsInstance  /*實際Exceptions*/;

    public static ExceptionsBuilder builder(ExceptionsInterface exceptionsInstance) {
        return new ExceptionsBuilder(exceptionsInstance);
    }

    public ExceptionsBuilder(ExceptionsInterface exceptionsInstance) {
        this.exceptionsInstance = exceptionsInstance;
        this.exceptionsInstance.setExceptionLevel(exceptionsInstance.getExceptionLevel());
    }


    public ExceptionsBuilder setCustomMessage(String customMessage) {
        this.customMessage = customMessage;
        return this;
    }

    public ExceptionsBuilder setErrorLog(String errorLog) {
        this.errorLog = errorLog;
        return this;
    }

    public ExceptionsBuilder setErrorLog(Object... errorLogs) {
        StringBuilder logsBuilder = new StringBuilder();
        for (Object obj : errorLogs) {
            logsBuilder.append(obj.toString()).append("\n");
        }
        this.errorLog = logsBuilder.toString();
        return this;
    }

    public ExceptionsBuilder setCause(Throwable cause) {
        this.cause = cause;
        return this;
    }

    public ExceptionsBuilder setIsPrintTrace(boolean isPrintTrace) {
        this.isPrintTrace = isPrintTrace;
        return this;
    }

    public Exceptions build() {
        Exceptions exceptions = (cause != null) ? new Exceptions(exceptionsInstance, cause) : new Exceptions(exceptionsInstance);
        exceptions.setCustomMessage(customMessage);
        exceptions.setErrorLog(errorLog);
        exceptions.setPrintTrace(isPrintTrace);
        return exceptions;
    }
}
