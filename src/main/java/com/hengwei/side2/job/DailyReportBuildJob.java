package com.hengwei.side2.job;

import com.hengwei.side2.service.QuartzJobService;
import com.hengwei.side2.utils.JobUtils;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DailyReportBuildJob extends QuartzJobBean {
    @Autowired
    private JobUtils jobUtils;
    @Autowired
    private QuartzJobService quartzJobService;
    private final Logger logger = LoggerFactory.getLogger((this.getClass().getName()));
    private final Lock lock = new ReentrantLock();

    @Override
    protected void executeInternal(JobExecutionContext context) {
        lock.lock();
        jobUtils.logStart(logger, this.getClass().getSimpleName());
        quartzJobService.dailyReportBuild();
        jobUtils.logFinish(logger, this.getClass().getSimpleName());
        lock.unlock();
    }
}

