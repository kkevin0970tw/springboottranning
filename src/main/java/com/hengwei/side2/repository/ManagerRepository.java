package com.hengwei.side2.repository;

import com.hengwei.side2.entity.ManagerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ManagerRepository extends JpaRepository<ManagerEntity, Integer> {
    Optional<ManagerEntity> findByAccount(String username);
}
