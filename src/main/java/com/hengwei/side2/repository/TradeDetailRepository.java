package com.hengwei.side2.repository;

import com.hengwei.side2.entity.TradeDetailEntity;
import com.hengwei.side2.entity.TradeDetailPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TradeDetailRepository extends JpaRepository<TradeDetailEntity, TradeDetailPK> {
    List<TradeDetailEntity> findAllByPkTradeId(Integer tradeId);
}
