package com.hengwei.side2.repository;

import com.hengwei.side2.entity.TradeStateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TradeStateRepository extends JpaRepository<TradeStateEntity, Integer> {
}
