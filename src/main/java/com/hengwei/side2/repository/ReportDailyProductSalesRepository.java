package com.hengwei.side2.repository;

import com.hengwei.side2.entity.ReportDailyProductSalesEntity;
import com.hengwei.side2.entity.ReportDailyProductSalesPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReportDailyProductSalesRepository extends JpaRepository<ReportDailyProductSalesEntity, ReportDailyProductSalesPK>, JpaSpecificationExecutor<ReportDailyProductSalesEntity> {
    List<ReportDailyProductSalesEntity> findAllByPkDate(LocalDate minusOneDay);
}
