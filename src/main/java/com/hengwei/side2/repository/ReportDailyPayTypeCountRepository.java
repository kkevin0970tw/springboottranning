package com.hengwei.side2.repository;

import com.hengwei.side2.entity.ReportDailyPayTypeCountEntity;
import com.hengwei.side2.entity.ReportDailyPayTypeCountPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReportDailyPayTypeCountRepository extends JpaRepository<ReportDailyPayTypeCountEntity, ReportDailyPayTypeCountPK> {
    List<ReportDailyPayTypeCountEntity> findAllByPkDate(LocalDate minusOneDay);
}
