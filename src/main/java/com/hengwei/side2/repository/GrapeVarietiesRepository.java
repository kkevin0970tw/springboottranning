package com.hengwei.side2.repository;

import com.hengwei.side2.entity.GrapeVarietiesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GrapeVarietiesRepository extends JpaRepository<GrapeVarietiesEntity, Integer> {
}
