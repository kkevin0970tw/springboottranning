package com.hengwei.side2.repository;

import com.hengwei.side2.entity.OakEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OakRepository extends JpaRepository<OakEntity, Integer> {
}
