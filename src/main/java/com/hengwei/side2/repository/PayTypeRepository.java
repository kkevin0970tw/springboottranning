package com.hengwei.side2.repository;

import com.hengwei.side2.entity.PayTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PayTypeRepository extends JpaRepository<PayTypeEntity, Integer> {
}
