package com.hengwei.side2.repository;

import com.hengwei.side2.entity.BrandEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BrandRepository extends JpaRepository<BrandEntity, Integer> {
    Optional<BrandEntity> existsByNameENAndNameTW(String nameEN, String nameTW);
}
