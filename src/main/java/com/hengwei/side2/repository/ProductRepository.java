package com.hengwei.side2.repository;

import com.hengwei.side2.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Integer>, PagingAndSortingRepository<ProductEntity, Integer> {

    @Query(nativeQuery = true, value = "SELECT  GetProductNextIdVal();")
    Integer getNextIdVal();

    boolean existsByBrandId(Integer brandId);

    Page<ProductEntity> findAllByCategoryId(Integer categoryId, PageRequest of);

    Page<ProductEntity> findAllByWineSubTypeId(Integer wineSubTypeId, PageRequest of);

    Page<ProductEntity> findAllByCountryId(Integer countryId, PageRequest of);

    Page<ProductEntity> findAllByAreaId(Integer areaId, PageRequest of);

    Page<ProductEntity> findAllByBrandId(Integer brandId, PageRequest of);

    Page<ProductEntity> findAllByGrapeVarietiesId(Integer grapeVarietiesId, PageRequest of);

    Page<ProductEntity> findAllByOakId(Integer oakId, PageRequest of);

    Page<ProductEntity> findAllByPriceBetween(Integer priceFloor, Integer priceCap, PageRequest of);

    @Query(nativeQuery = true,value = "SELECT fld_name FROM tbl_product WHERE fld_id = :productId")
    Optional<String> findNameById(@Param("productId")Integer productId);

}
