package com.hengwei.side2.repository;

import com.hengwei.side2.entity.TradeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface TradeRepository extends JpaRepository<TradeEntity, Integer> {
    Optional<TradeEntity> findByIdAndTradeDateTimeBetween(Integer tradeId, LocalDateTime beginDate, LocalDateTime endDate);

    @Query(value = " SELECT " +
            " CAST(COUNT(*) AS DECIMAL) AS totalCount, " +
            " CAST(SUM(t.fld_trade_amount) AS DECIMAL) AS totalTradeAmount, " +
            " CAST(SUM(t.fld_trade_amount_with_discount) AS DECIMAL) AS totalTradeAmountWithDiscount " +
            " FROM tbl_trade t WHERE t.fld_trade_datetime BETWEEN :startDateTime AND :endDateTime ",
            nativeQuery = true)
    Object[][] getTradeSummaryByDateRange(@Param("startDateTime") LocalDateTime todayStart, @Param("endDateTime") LocalDateTime todayEnd);

    @Query(value = " SELECT " +
            " t.fld_pay_type_id AS payTypeId, " +
            " CAST(COUNT(*) AS DECIMAL ) AS count " +
            " FROM tbl_trade t WHERE t.fld_trade_datetime BETWEEN :startDateTime AND :endDateTime " +
            " GROUP BY t.fld_pay_type_id ",
            nativeQuery = true)
    Object[][] getPayTypeCountByDateRange(@Param("startDateTime") LocalDateTime todayStart, @Param("endDateTime") LocalDateTime todayEnd);

    @Query(value = "SELECT " +
            " CAST(ttd.fld_product_id AS DECIMAL ) AS productId, " +
            " SUM(ttd.fld_quantity) AS count, " +
            " CAST(SUM(ttd.fld_price * ttd.fld_discount * 0.01 * ttd.fld_quantity) AS DECIMAL ) AS totalAmount " +
            " FROM tbl_trade_detail ttd " +
            " WHERE ttd.fld_trade_id IN ( " +
            "    SELECT fld_id " +
            "    FROM tbl_trade t " +
            "    WHERE t.fld_trade_datetime BETWEEN :startDateTime AND :endDateTime " +
            " ) " +
            " GROUP BY ttd.fld_product_id " +
            " ORDER BY ttd.fld_product_id",
            nativeQuery = true)
    Object[][] getProductSalesByDateRange(@Param("startDateTime") LocalDateTime todayStart, @Param("endDateTime") LocalDateTime todayEnd);

    @Query(value = "SELECT " +
            " CAST(t.fld_trade_state_id AS DECIMAL ) AS tradeStateId, " +
            " CAST(COUNT(*) AS DECIMAL ) AS count," +
            " CAST(SUM(t.fld_trade_amount) AS DECIMAL ) AS totalAmount, " +
            " CAST(SUM(t.fld_trade_amount_with_discount) AS DECIMAL ) AS totalAmountWithDiscount " +
            " FROM tbl_trade t " +
            " WHERE t.fld_trade_datetime BETWEEN :startDateTime AND :endDateTime " +
            " GROUP BY t.fld_trade_state_id ",
            nativeQuery = true)
    Object[][] getTradeStateByDateRange(@Param("startDateTime") LocalDateTime todayStart, @Param("endDateTime") LocalDateTime todayEnd);
}
