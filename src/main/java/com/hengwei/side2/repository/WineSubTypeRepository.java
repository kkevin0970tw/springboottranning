package com.hengwei.side2.repository;

import com.hengwei.side2.entity.WineSubTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WineSubTypeRepository extends JpaRepository<WineSubTypeEntity, Integer> {
}
