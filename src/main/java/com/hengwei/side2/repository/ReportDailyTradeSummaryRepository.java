package com.hengwei.side2.repository;

import com.hengwei.side2.entity.ReportDailyTradeSummaryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface ReportDailyTradeSummaryRepository extends JpaRepository<ReportDailyTradeSummaryEntity, LocalDate> {
}
