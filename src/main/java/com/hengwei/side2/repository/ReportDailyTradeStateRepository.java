package com.hengwei.side2.repository;

import com.hengwei.side2.entity.ReportDailyTradeStateEntity;
import com.hengwei.side2.entity.ReportDailyTradeStatePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReportDailyTradeStateRepository extends JpaRepository<ReportDailyTradeStateEntity, ReportDailyTradeStatePK> {
    List<ReportDailyTradeStateEntity> findAllByPkDate(LocalDate minusOneDay);
}
