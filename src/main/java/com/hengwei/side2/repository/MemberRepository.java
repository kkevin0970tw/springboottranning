package com.hengwei.side2.repository;

import com.hengwei.side2.entity.MemberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MemberRepository extends JpaRepository<MemberEntity, Integer> {

    List<MemberEntity> findAllByRoleId(Integer roleId);

    Optional<MemberEntity> findByAccount(String account);

    Optional<MemberEntity> findByIdAndAccountAndPassword(Integer id, String account, String oldPassword);

    Optional<MemberEntity> findByAccountAndPassword(String account, String password);

    Optional<MemberEntity> findByIdAndAccount(Integer id, String account);

    @Query(nativeQuery = true, value = "SELECT MAX(fld_id) from tbl_member")
    Integer findMaxId();
}
