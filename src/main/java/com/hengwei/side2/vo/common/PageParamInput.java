package com.hengwei.side2.vo.common;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name = "頁次")
public class PageParamInput {

    /*查詢頁次*/
    @Schema(defaultValue = "1")
    private Integer targetPage;

    /*單頁筆數*/
    @Schema(defaultValue = "10")
    private Integer perPageQuantity;
}
