package com.hengwei.side2.vo.common;

import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class PageParamOutput {
    /*查詢頁次*/
    private Integer targetPage;

    /*查詢頁次實際筆數*/
    private Integer targetPageQuantity;

    /*單頁筆數*/
    private Integer perPageQuantity;

    /*總頁次*/
    private Integer totalPage;

    /*資料總筆數*/
    private Long totalQuantity;

    public void setPageParamOutput(Integer targetPage, Integer targetPageQuantity, Integer perPageQuantity, Integer totalPage, Long totalQuantity) {
        this.targetPage = targetPage;
        this.targetPageQuantity = targetPageQuantity;
        this.perPageQuantity = perPageQuantity;
        this.totalPage = totalPage;
        this.totalQuantity = totalQuantity;
    }

    public void setPageParamOutput(Page page) {
        this.targetPage = page.getPageable().getPageNumber() + 1;
        this.targetPageQuantity = page.getContent().size();
        this.perPageQuantity = page.getPageable().getPageSize();
        this.totalPage = page.getTotalPages();
        this.totalQuantity = page.getTotalElements();
    }
}
