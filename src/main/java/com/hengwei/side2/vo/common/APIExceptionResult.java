package com.hengwei.side2.vo.common;

import com.hengwei.side2.exception.Exceptions;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class APIExceptionResult {
    private int code = -1;
    private String exceptionsMessage;
    private String customMessage = "Exception Happen";

    public APIExceptionResult(Exceptions exceptions) {
        code = exceptions.getCode();
        exceptionsMessage = exceptions.getMessage();
        customMessage = exceptions.getCustomMessage();
    }

    public APIExceptionResult(Exception exception) {
        exceptionsMessage = exception.getMessage();
        customMessage=exception.toString();
    }
}
