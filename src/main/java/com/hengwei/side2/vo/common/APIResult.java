package com.hengwei.side2.vo.common;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class APIResult<T> {

    private int code = 0;
    private String message = "SUCCESS";
    private T data;


    public APIResult(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public APIResult(T data) {
        this.data = data;
    }
}
