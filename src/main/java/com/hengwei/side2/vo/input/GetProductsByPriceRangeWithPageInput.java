package com.hengwei.side2.vo.input;

import com.hengwei.side2.enumeration.SortEnum;
import com.hengwei.side2.vo.common.PageParamInput;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name = "產品-價格區間")
public class GetProductsByPriceRangeWithPageInput extends PageParamInput {

    @Schema(defaultValue = "50000")
    private Integer priceCap;

    @Schema(defaultValue = "1")
    private Integer priceFloor;

    @Schema(defaultValue = "ASC")
    private String sort = SortEnum.ASC.name();
}
