package com.hengwei.side2.vo.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@Schema(name = "品牌-查詢單筆輸入")
public class GetBrandByIdInput {

    @Schema(description = "品牌ID", defaultValue = "1")
    @NotNull
    private Integer id;

}
