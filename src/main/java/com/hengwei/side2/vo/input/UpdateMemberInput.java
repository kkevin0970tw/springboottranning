package com.hengwei.side2.vo.input;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Schema(name = "會員-更新輸入")
public class UpdateMemberInput {

    @NotNull
    @Schema(defaultValue = "1")
    private Integer id;

    @NotEmpty
    @Schema(defaultValue = "Taylor Dayne")
    private String name;

    @NotEmpty
    @Pattern(regexp = "/^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z]+$/")
    @Schema(defaultValue = "asfhzhf324hsdfhas@gmail.com")
    private String email;

    @NotEmpty
    @Pattern(regexp = "09\\d{2}(\\d{6}|-\\d{3}-\\d{3})",message = "格式 : 09xxxxxxxx")
    @Schema(defaultValue = "0912345678")
    private String phoneNumber;

    private LocalDate dateOfBirth = LocalDate.now();

    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9]{8,20}$", message = "大小寫英文字母 與 數字 最少8碼 最多20碼")
    @Schema(defaultValue = "root")
    private String account;

    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9]{8,20}$", message = "大小寫英文字母 與 數字 最少8碼 最多20碼")
    @Schema(defaultValue = "root")
    private String password;

    @JsonIgnore
    private String profilePicture = "default/no-pic.jpg";

}
