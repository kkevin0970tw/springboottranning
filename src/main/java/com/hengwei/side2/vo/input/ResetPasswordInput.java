package com.hengwei.side2.vo.input;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ResetPasswordInput {

    @NotNull
    private Integer id;

    @NotEmpty
    private String account;

    @NotEmpty
    private String oldPassword;

    @NotEmpty
    private String newPassword;

}
