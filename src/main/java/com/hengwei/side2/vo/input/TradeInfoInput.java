package com.hengwei.side2.vo.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDate;

@Data
@Schema(name = "訂單查詢 輸入")
public class TradeInfoInput {

    @Schema(description = "訂單編號", defaultValue = "10")
    @NotNull
    private Integer tradeId;

    @Schema(description = "起始時間(含)", defaultValue = "2024-01-01")
    @NotNull
    private LocalDate beginDate = LocalDate.MIN;

    @Schema(description = "結束時間(含)")
    @NotNull
    private LocalDate endDate = LocalDate.now();

    @Schema(description = "是否包含細節", defaultValue = "false", allowableValues = {"true", "false"})
    @NotNull
    private boolean isIncludeDetail;
}
