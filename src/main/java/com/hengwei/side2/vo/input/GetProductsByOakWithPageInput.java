package com.hengwei.side2.vo.input;

import com.hengwei.side2.vo.common.PageParamInput;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name = "產品-橡木桶ID輸入")
public class GetProductsByOakWithPageInput extends PageParamInput {

    @Schema(defaultValue = "1")
    private Integer oakId;
}
