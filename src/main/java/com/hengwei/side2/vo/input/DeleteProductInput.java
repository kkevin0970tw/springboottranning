package com.hengwei.side2.vo.input;

import lombok.Data;

@Data
public class DeleteProductInput {
    private Integer id;
}
