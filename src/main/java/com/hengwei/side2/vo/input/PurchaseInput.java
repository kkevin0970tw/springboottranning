package com.hengwei.side2.vo.input;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class PurchaseInput {
    @Schema(description = "購買會員ID", defaultValue = "35")
    @NotNull
    private Integer memberId;

    @Schema(description = "交易時間")
    @Hidden
    private LocalDateTime tradeDateTime = LocalDateTime.now();

    @Schema(description = "購買細項")
    private List<PurchaseItem> purchaseItemList;

    @Schema(description = "付款方式ID", defaultValue = "3")
    private Integer payTypeId;

    @Data
    public static class PurchaseItem {
        @Schema(description = "購買產品ID", defaultValue = "75")
        private Integer productId;
        @Schema(description = "購買當下原價", defaultValue = "424")
        private Integer price;
        @Schema(description = "購買當下折數", defaultValue = "100")
        private Integer discount;
        @Schema(description = "購買當下售價", defaultValue = "424")
        private Integer priceWithDiscount;
        @Schema(description = "購買數量", defaultValue = "10")
        private Integer quantity;
    }
}
