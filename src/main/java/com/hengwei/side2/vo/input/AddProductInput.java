package com.hengwei.side2.vo.input;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AddProductInput {

    @Schema(defaultValue = "test1")
    private String name;

    @Schema(defaultValue = "1")
    private Integer categoryId;

    @Schema(defaultValue = "1")
    private Integer wineSubTypeId;

    @Schema(defaultValue = "1")
    private Integer countryId;

    @Schema(defaultValue = "1")
    private Integer areaId;

    @Schema(defaultValue = "1")
    private Integer brandId;

    @Schema(defaultValue = "1")
    private Integer grapeVarietiesId;

    @Schema(defaultValue = "1")
    private Integer oakId;

    @Schema(defaultValue = "test.jpg")
    private String imgPath;

    @Schema(defaultValue = "111")
    private Integer price;

    @Schema(defaultValue = "111")
    private Short capacity;

    @Schema(defaultValue = "test add")
    private String description;

    @Schema(defaultValue = "true")
    private Boolean show;

    @Schema(defaultValue = "100")
    private Integer discount;

    @Hidden
    private LocalDateTime updateDateTime = LocalDateTime.now();
}
