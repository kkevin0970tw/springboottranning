package com.hengwei.side2.vo.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class GetMembersByRoleIdInput {

    @Schema(description = "角色ID", defaultValue = "5")
    @DecimalMax("5")
    @DecimalMin("1")
    @NotNull
    private Integer roleId;
}
