package com.hengwei.side2.vo.input;

import com.hengwei.side2.vo.common.PageParamInput;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class GetProductsByAreaWithPageInput extends PageParamInput {

    @Schema(defaultValue = "1")
    private Integer areaId;
}
