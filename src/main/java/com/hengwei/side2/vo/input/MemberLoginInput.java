package com.hengwei.side2.vo.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Schema(name = "會員-登入")
public class MemberLoginInput {

    @NotEmpty
    @Schema(defaultValue = "root1234")
    private String account;

    @NotEmpty
    @Schema(defaultValue = "root1234")
    private String password;

    @Schema(defaultValue = "false")
    private boolean isRemembered = false;
}
