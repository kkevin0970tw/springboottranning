package com.hengwei.side2.vo.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@Schema(name = "品牌-刪除輸入")
public class DeleteBrandInput {

    @Schema(description = "品牌ID", defaultValue = "99999")
    @NotNull
    private Integer id;
}
