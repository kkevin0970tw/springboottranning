package com.hengwei.side2.vo.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Size;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Data
@Schema(name = "產品銷售報表 輸入")
public class GenerateProductSalesReportInput {
    @Schema(description = "起始日")
    private LocalDate starDate;
    @Schema(description = "結束日")
    private LocalDate endDate;
    @Size(min = 1,max = 10,message = "最少1筆，最多10筆")
    private Integer[] productIdArray;
}
