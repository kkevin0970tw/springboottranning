package com.hengwei.side2.vo.input;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Schema(name = "會員-新增輸入")
public class AddMemberInput {

    @Schema(description = "會員名稱", defaultValue = "Taylor Dayne")
    @NotEmpty
    private String name;

    @Schema(description = "會員EMAIL", defaultValue = "asfhzhf324hsdfhas@gmail.com")
    @Email
    @NotEmpty
    private String email;

    @Pattern(regexp = "09\\d{2}(\\d{6}|-\\d{3}-\\d{3})", message = "格式 : 09xxxxxxxx")
    @Schema(description = "會員手機", defaultValue = "0912345678")
    @NotEmpty
    private String phoneNumber;

    @Schema(description = "會員生日")
    private LocalDate dateOfBirth = LocalDate.now();

    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9]{8,20}$", message = "大小寫英文字母 與 數字 最少8碼 最多20碼")
    @Schema(defaultValue = "root1234")
    private String account;

    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9]{8,20}$", message = "大小寫英文字母 與 數字 最少8碼 最多20碼")
    @Schema(defaultValue = "root5678")
    private String password;

    private MultipartFile imgFile;


    @JsonIgnore
    private LocalDateTime registrationDate = LocalDateTime.now();
    @JsonIgnore
    private LocalDateTime lastLogin = LocalDateTime.now();
    @JsonIgnore
    private String profilePicture = "default/no-pic.jpg";
    @JsonIgnore
    private int roleId = 1;
}
