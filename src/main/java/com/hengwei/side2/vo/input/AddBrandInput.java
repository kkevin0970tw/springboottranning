package com.hengwei.side2.vo.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Schema(name = "品牌-新增輸入")
public class AddBrandInput {

    @Schema(description = "品牌 英文名稱" ,defaultValue = "Tesla")
    @NotEmpty
    private String nameEN;

    @Schema(description = "品牌 中文名稱" ,defaultValue = "特斯拉")
    @NotEmpty
    private String nameTW;
}
