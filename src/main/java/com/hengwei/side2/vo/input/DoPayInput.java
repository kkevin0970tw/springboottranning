package com.hengwei.side2.vo.input;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class DoPayInput {

    @Schema(description = "付款方式 ID" , defaultValue = "2")
    @NotNull
    private Integer payTypeId;

    @Schema(description = "付款總額" , defaultValue = "200")
    @NotNull
    private Integer payAmount;

    @Schema(description = "交易訂單 ID" , defaultValue = "2")
    @NotNull
    private Integer tradeId;

    @Hidden
    private String authorizationKey;  /* 未來調整 暫不使用*/

}
