package com.hengwei.side2.vo.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class GetMemberByIdInput {

    @Schema(description = "會員ID", defaultValue = "99999")
    @NotNull
    private Integer id;
}
