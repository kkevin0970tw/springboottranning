package com.hengwei.side2.vo.output;

import com.hengwei.side2.entity.MemberEntity;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class GetMemberByIdOutput {
    public GetMemberByIdOutput(MemberEntity memberEntity) {
        BeanUtils.copyProperties(memberEntity, this);
    }

    private Integer id;
    private String name;
    private String email;
    private String phoneNumber;
    private LocalDate dateOfBirth;
    private LocalDateTime registrationDate;
    private LocalDateTime lastLogin;
    private String profilePicture;
    private int roleId;
    private String account;
    private String password;
}
