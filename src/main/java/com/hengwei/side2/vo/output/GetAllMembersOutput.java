package com.hengwei.side2.vo.output;

import com.hengwei.side2.entity.MemberEntity;
import jakarta.persistence.Column;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class GetAllMembersOutput {
    public GetAllMembersOutput(List<OutputData> outputDataList) {
        setOutputDataList(outputDataList);
    }

    private List<OutputData> outputDataList;

    @Data
    public static class OutputData {
        private Integer id;
        private String name;
        private String email;
        private String phoneNumber;
        private LocalDate dateOfBirth;
        private LocalDateTime registrationDate;
        private LocalDateTime lastLogin;
        private String profilePicture;
        private int roleId;
        private String account;
        private String password;

        public OutputData(MemberEntity entity) {
            BeanUtils.copyProperties(entity, this);
        }
    }
}
