package com.hengwei.side2.vo.output;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class ResetPasswordOutput {
    private Integer id;
    private String name;
    private String email;
    private String phoneNumber;
    private LocalDate dateOfBirth;
    private LocalDateTime registrationDate;
    private LocalDateTime lastLogin;
    private String profilePicture;
    private int roleId;
    private String account;
    private String password;
}
