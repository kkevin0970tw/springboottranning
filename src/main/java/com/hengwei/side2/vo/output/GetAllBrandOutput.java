package com.hengwei.side2.vo.output;

import com.hengwei.side2.entity.BrandEntity;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.List;

@Data
public class GetAllBrandOutput {
    public GetAllBrandOutput(List<OutputData> outputDataList) {
        setOutputDataList(outputDataList);
    }

    private List<OutputData> outputDataList;

    @Data
    public static class OutputData {
        public OutputData(BrandEntity entity) {
            BeanUtils.copyProperties(entity, this);
        }

        private String nameEN;
        private String nameTW;
    }
}
