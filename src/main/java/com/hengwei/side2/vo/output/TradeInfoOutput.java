package com.hengwei.side2.vo.output;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class TradeInfoOutput {
    private Trade trade;
    private List<TradeDetail> tradeDetailList;

    @Data
    public static class Trade {
        private Integer memberId;
        private Integer tradeId;
        private Integer tradeAmount;
        private Integer tradeAmountWithDiscount;
        private String tradeState;
        private LocalDateTime tradeDateTime;
        private String payType;
    }

    @Data
    public static class TradeDetail {
        private Integer productId;
        private Integer price;
        private Integer discount;
        private Integer quantity;
    }
}
