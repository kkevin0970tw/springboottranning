package com.hengwei.side2.vo.output;

import com.hengwei.side2.entity.TradeDetailEntity;
import com.hengwei.side2.entity.TradeEntity;
import lombok.Data;

import java.util.List;

@Data
public class PurchaseOutput {
    private TradeEntity trade;
    private List<TradeDetailEntity> tradeDetailList;
}
