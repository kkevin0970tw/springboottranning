package com.hengwei.side2.vo.output;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AddProductOutput {
    private Integer id;
    private String name;
    private Integer categoryId;
    private Integer wineSubTypeId;
    private Integer countryId;
    private Integer areaId;
    private Integer brandId;
    private Integer grapeVarietiesId;
    private Integer oakId;
    private String imgPath;
    private Integer price;
    private Short capacity;
    private String description;
    private Boolean show;
    private LocalDateTime updateDateTime;
}
