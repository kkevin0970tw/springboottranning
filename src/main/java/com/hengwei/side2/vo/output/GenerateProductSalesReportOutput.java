package com.hengwei.side2.vo.output;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenerateProductSalesReportOutput {

    private List<OutputData> outPutDataList;

    @Data
    public static class OutputData {
        private LocalDate date;
        private String productName;
        private BigDecimal count;
        private BigDecimal totalAmount;
    }

}
