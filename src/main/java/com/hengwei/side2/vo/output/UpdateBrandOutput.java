package com.hengwei.side2.vo.output;

import com.hengwei.side2.entity.BrandEntity;
import lombok.Data;

@Data
public class UpdateBrandOutput {
    private int id;
    private String nameEN;
    private String nameTW;
}
