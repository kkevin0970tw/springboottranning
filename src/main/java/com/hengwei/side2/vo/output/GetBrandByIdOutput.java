package com.hengwei.side2.vo.output;

import com.hengwei.side2.entity.BrandEntity;
import lombok.Data;
import org.springframework.beans.BeanUtils;

@Data
public class GetBrandByIdOutput {
    public GetBrandByIdOutput(BrandEntity brandEntity) {
        BeanUtils.copyProperties(brandEntity, this);
    }

    private String nameEN;
    private String nameTW;
}
