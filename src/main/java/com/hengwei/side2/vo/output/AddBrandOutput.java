package com.hengwei.side2.vo.output;

import com.hengwei.side2.entity.BrandEntity;
import lombok.Data;

@Data
public class AddBrandOutput {
    private int id;
    private String nameEN;
    private String nameTW;
}
