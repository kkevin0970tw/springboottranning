package com.hengwei.side2.vo.output;

import com.hengwei.side2.vo.common.PageParamOutput;
import lombok.Data;

import java.util.List;

@Data
public class GetProductsByAreaWithPageOutput extends PageParamOutput {
    private List<BasicProductOutput> dataList;
}
