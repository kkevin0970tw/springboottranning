package com.hengwei.side2.vo.output;

import com.hengwei.side2.entity.*;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BasicProductOutput {
    private Integer id;
    private String name;
    private CategoryEntity category;
    private WineSubTypeEntity wineSubType;
    private CountryEntity country;
    private AreaEntity area;
    private BrandEntity brand;
    private GrapeVarietiesEntity grapeVarieties;
    private OakEntity oak;
    private String imgPath;
    private Integer price;
    private Short capacity;
    private String description;
    private Boolean show;
    private LocalDateTime updateDateTime;
    private Integer discount;
}
