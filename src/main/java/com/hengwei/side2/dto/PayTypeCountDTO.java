package com.hengwei.side2.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PayTypeCountDTO {
    private Byte payTypeId;
    private BigDecimal count;

}
