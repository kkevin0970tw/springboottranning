package com.hengwei.side2.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TradeSummaryDTO implements Serializable {
    private BigDecimal totalCount;
    private BigDecimal totalTradeAmount;
    private BigDecimal totalTradeAmountWithDiscount;
}
