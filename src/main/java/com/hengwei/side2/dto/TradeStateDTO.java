package com.hengwei.side2.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TradeStateDTO {
    private BigDecimal tradeStateId;
    private BigDecimal count;
    private BigDecimal totalAmount;
    private BigDecimal totalAmountWithDiscount;
}
