package com.hengwei.side2.enumeration;

import com.hengwei.side2.exception.Exceptions;
import com.hengwei.side2.exception.ExceptionsBuilder;

import java.util.ArrayList;

public enum RoleEnum {
    ROOT(7),
    ADMIN(6),
    USER_5(5),
    USER_4(4),
    USER_3(3),
    USER_2(2),
    USER_1(1),
    ;

    private final int roleId;

    RoleEnum(int roleId) {
        this.roleId = roleId;
    }

    public static RoleEnum getRoleByRoleId(int roleId) {
        validRoleId(roleId);
        RoleEnum roleEnum = null;
        switch (roleId) {
            case 1 -> roleEnum = RoleEnum.USER_1;
            case 2 -> roleEnum = RoleEnum.USER_2;
            case 3 -> roleEnum = RoleEnum.USER_3;
            case 4 -> roleEnum = RoleEnum.USER_4;
            case 5 -> roleEnum = RoleEnum.USER_5;
            case 6 -> roleEnum = RoleEnum.ADMIN;
            case 7 -> roleEnum = RoleEnum.ROOT;
        }
        return roleEnum;
    }

    public static String[] getSecurityAuthority(int roleId) {
        validRoleId(roleId);
        int roleStringArrayLength = 7 - roleId + 1;
        String[] roleStringArray = new String[roleStringArrayLength];
        int index = 0;
        for (var role : RoleEnum.values()) {
            if (roleId <= role.getRoleId()) {
                roleStringArray[index] = role.name();
            }
            index++;
        }
        return roleStringArray;
    }

    public int getRoleId() {
        return roleId;
    }

    /*取得 當前權限以及其以下的所有權限  如會員為 USER_3 ， 則USER_2、USER_1 該有的權限 USER_3也應當有，故需回傳*/
    public static ArrayList<String> getAllRoleByRoleId(int roleId) {
        validRoleId(roleId);
        var roleList = new ArrayList<String>();
        for (RoleEnum value : RoleEnum.values()) {
            if (roleId >= value.getRoleId()) {
                roleList.add(value.name());
            }
        }
        return roleList;
    }

    private static void validRoleId(int roleId) {
        if (roleId < 1 || roleId > 7)
            throw ExceptionsBuilder.builder(Exceptions.AuthorizationException.AUTHORIZATION_EXCEPTION)
                    .setErrorLog("暫時使用，為新增對應Exception，未來待調")
                    .build();
    }
}
