package com.hengwei.side2.enumeration;

import lombok.Getter;

@Getter
public enum PayTypeEnum {
    CASH("現金", "Cash"),
    CREDIT_CARD("信用卡", "Credit Card"),
    BANK_TRANSFER("轉帳", "Bank Transfer"),
    CASH_ON_DELIVERY("貨到付款", "Cash on Delivery"),
    LINE_PAY("LinePay", "LinePay"),
    ALI_PAY("支付寶", "Alipay");

    private final String twName;
    private final String enName;

    PayTypeEnum(String twName, String enName) {
        this.twName = twName;
        this.enName = enName;
    }
}