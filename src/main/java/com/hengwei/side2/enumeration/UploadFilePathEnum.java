package com.hengwei.side2.enumeration;

import lombok.Getter;

@Getter
public enum UploadFilePathEnum {

    PROFILE_IMG_PATH("profile//");

    private String path;

    UploadFilePathEnum(String path) {
        this.path = path;
    }
}
