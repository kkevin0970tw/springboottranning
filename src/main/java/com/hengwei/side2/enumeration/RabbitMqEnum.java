package com.hengwei.side2.enumeration;

import lombok.Getter;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.TopicExchange;

@Getter
public enum RabbitMqEnum {

    PRODUCT_ADDED("productAddedExchange", DirectExchange.class, "user.notification", "userQueue"),
    PRODUCT_EVENT("productEventExchange", TopicExchange.class, "product.*",  "managerQueue"),
    REPORT_GENERATED("reportGeneratedExchange", DirectExchange.class, "report.generated", "managerQueue"),
    MAINTENANCE_EVENT("maintenanceEventExchange", FanoutExchange.class, "", "userQueue", "managerQueue");
    private final String exchangeName;
    private final Class<?> exchangeType;
    private final String routeKey;
    private final String[] queueNames;


    RabbitMqEnum(String exchangeName, Class<?> exchangeType, String routeKey, String... queueNames) {
        this.exchangeName = exchangeName;
        this.exchangeType = exchangeType;
        this.routeKey = routeKey;
        this.queueNames = queueNames;
    }
}
