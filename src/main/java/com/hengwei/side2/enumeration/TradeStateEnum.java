package com.hengwei.side2.enumeration;

import lombok.Getter;
import lombok.Setter;

public enum TradeStateEnum {


    CANCELLED(1),
    UNPAID(2),
    PAID(3),
    PENDING_SHIPMENT(4),
    SHIPPED(5),
    COMPLETED(6),
    REFUNDING(7),
    REFUNDED(8),
    ;

    @Getter
    @Setter
    private int stateNum;

    TradeStateEnum(int stateNum) {
        this.stateNum = stateNum;
    }

}
