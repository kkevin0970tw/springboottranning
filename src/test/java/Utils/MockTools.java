package Utils;

import com.hengwei.side2.exception.Exceptions;
import com.hengwei.side2.exception.ExceptionsBuilder;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.lang.reflect.Field;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class MockTools extends Mockito {
    public MvcResult mockHttpGet(MockMvc mockMvc, String url, Object input, ResultMatcher matcher) {
        try {
            return mockMvc.perform(get(url)
                            .params(getParamMap(input)))
                    .andExpect(matcher)
                    .andReturn();
        } catch (Exception e) {
            throw ExceptionsBuilder.builder(Exceptions.TestException.TEST_EXCEPTION).build();
        }
    }

    public MvcResult mockHttpPost(MockMvc mockMvc, String url, String content, ResultMatcher matcher) {
        try {
            return mockMvc.perform(post(url)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(content))
                    .andExpect(matcher)
                    .andReturn();
        } catch (Exception e) {
            throw ExceptionsBuilder.builder(Exceptions.TestException.TEST_EXCEPTION).build();
        }
    }

    private static MultiValueMap<String, String> getParamMap(Object input) {
        Field[] fields = input.getClass().getDeclaredFields();
        Object tempValue;

        MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<>();
        for (Field field : fields) {
            field.setAccessible(true); /*因屬性皆為 private ，故透過此方法取得，只得使用在開發環境*/
            try {
                tempValue = field.get(input);
                paramMap.add(field.getName(), tempValue.toString());
            } catch (IllegalAccessException e) {
                throw ExceptionsBuilder.builder(Exceptions.TestException.TEST_CONVERT_PARAM_FAIL).build();
            }
        }
        return paramMap;
    }
}
