package service;


import com.google.gson.reflect.TypeToken;
import com.hengwei.side2.Side2Application;
import com.hengwei.side2.vo.common.APIResult;
import com.hengwei.side2.vo.input.GetBrandByIdInput;
import com.hengwei.side2.vo.output.GetAllBrandOutput;
import com.hengwei.side2.vo.output.GetBrandByIdOutput;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.io.UnsupportedEncodingException;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = Side2Application.class)
@AutoConfigureMockMvc
public class BrandServiceTest implements TestServiceInterface{
    @Autowired
    private MockMvc mockMvc;
    private final String prefix_url = API_PREFIX_BRAND;
    private String api_path;

//    @Test
//    void test_getBrandById() throws UnsupportedEncodingException {
//        api_path = "";
//
//        GetBrandByIdInput input = new GetBrandByIdInput();
//        input.setId(1);
//
//        String responseJson = mockTools.mockHttpGet(mockMvc, prefix_url + api_path, input, status().isOk())
//                .getResponse()
//                .getContentAsString();
//
//        APIResult<GetBrandByIdOutput> result = gson.fromJson(responseJson, new TypeToken<APIResult<GetAllBrandOutput>>() {
//        }.getType());
//
//        Assertions.assertEquals(0, result.getCode());
//    }
}
