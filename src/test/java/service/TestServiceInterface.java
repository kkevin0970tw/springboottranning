package service;

import Utils.MockTools;
import com.google.gson.Gson;

public interface TestServiceInterface {
    MockTools mockTools = new MockTools();
    Gson gson = new Gson();
    String API_PREFIX_BRAND = "//api/brand";
    String API_PREFIX_AREA = "//api/area";
    String API_PREFIX_PRODUCT = "//api/product";

}
